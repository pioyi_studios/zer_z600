/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _PI_HASHMAP_H
#define _PI_HASHMAP_H

#include <stdlib.h>

// Should be a perfect power of two in order to simplify the modulo operation
#define PI_HASHMAP_INITIAL_CAPACITY 16

// I'm leaving both fields as generic variables
// Both of these should be heap allocated though
typedef struct
{
    void *key;
    void *value;
} pi_hashmap_entry_t;

typedef struct
{
    pi_hashmap_entry_t *entries;
    
    size_t capacity;
    size_t length;
} pi_hashmap_t;

void pi_hashmap_create(pi_hashmap_t *hashmap);

void* pi_hashmap_find_value_from_string(pi_hashmap_t *hashmap, const char *key);
void pi_hashmap_insert_new_string(pi_hashmap_t *hashmap, const char *key, void *value);

#endif
