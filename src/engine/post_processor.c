/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "post_processor.h"
#include "sprite.h"

void pi_post_processor_create(pi_post_processor_t *processor, int width, int height)
{
    // First, I need to create a framebuffer object
    glGenFramebuffers(1, &processor->frame_buffer);
    glBindFramebuffer(GL_FRAMEBUFFER, processor->frame_buffer);

    // I should then attach a texture to capture screen colors
    glGenTextures(1, &processor->internal_texture);
    glBindTexture(GL_TEXTURE_2D, processor->internal_texture);
    // Initialize level 0. The border value (right after the height) must always be set to zero
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Attach level 0 of the texture onto the framebuffer object
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, processor->internal_texture, 0);

    pi_post_processor_disable(processor);
}

void pi_post_processor_begin(pi_post_processor_t *processor)
{
    if (processor->shader != NULL)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, processor->frame_buffer);
    }
}

void pi_post_processor_end(pi_post_processor_t *processor)
{
    // If the processor is active, render the texture onto the normal screen
    if (processor->shader != NULL)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glUseProgram(processor->shader->id);
        glActiveTexture(GL_TEXTURE0);
        glBindVertexArray(pi_root_vao);
        glBindTexture(GL_TEXTURE_2D, processor->internal_texture);
        // Just render the quad
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
}

void pi_post_processor_activate(pi_post_processor_t *processor, pi_program_t *program)
{
    processor->shader = program;
    glBindFramebuffer(GL_FRAMEBUFFER, processor->frame_buffer);
    glClear(GL_COLOR_BUFFER_BIT);
}

void pi_post_processor_disable(pi_post_processor_t *processor)
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    processor->shader = NULL;
}
