#version 430

layout (location = 0) out vec4 f_color;
uniform float transparency;
uniform float time;

in vec2 tex_coords;

void main()
{
    // Crop out the sections that do not match the wave
    if (tex_coords.y < 0.15 && tex_coords.y < 0.15 * abs(sin(3 * time + 4 * tex_coords.x)))
        discard;
	
    f_color = vec4(0, 0, 0, 1.0 - transparency);
}
