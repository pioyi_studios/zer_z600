#version 430

layout (location = 0) out vec4 f_color;

in vec2 tex_coords;
uniform sampler2D sprite_texture;
uniform float transparency;

void main()
{
    f_color = texture(sprite_texture, tex_coords);
    
    if (f_color.a < 0.1)
        discard;

    f_color.a = 1.0 - transparency;
}
