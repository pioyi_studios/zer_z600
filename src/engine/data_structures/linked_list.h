/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _PI_LINKED_LIST_H
#define _PI_LINKED_LIST_H

typedef struct pi_linked_node_t
{
    // Should be heap-allocated
    void *data;
    
    struct pi_linked_node_t *next;
} pi_linked_node_t;

typedef pi_linked_node_t* pi_linked_list_t;

void pi_linked_list_insert(pi_linked_list_t *list, void *data);
// The provided data field should be deleted by the user!
void pi_linked_list_delete(pi_linked_list_t *list, void *data);

#endif
