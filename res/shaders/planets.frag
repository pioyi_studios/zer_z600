#version 430

layout (location = 0) out vec4 f_color;

in vec2 tex_coords;
uniform sampler2D sprite_texture;
flat in int instance_id;

float get_rgb_component(int hue, float saturation)
{
    /*
     * This values are not random, they are produced by dividing the hsv plane into four distinct regions on the hue axis
     * For each color component just draw a function that describes its concentration as the hue changes
     * The function is even, so I can just get the absolute value instead
     */
    float value = abs(hue % 360 - 180);

    if (value >= 120)
        return 1.0;
    
    else if (value > 60)
        return 1 - saturation + (value - 60) / 60.0 * saturation;

    // The base of the graph gets lifted up based on the saturation
    // More specifically, lower saturation tends to squish the function's max and min values together
    return 1 - saturation;
}

// Convert hue-saturation-value colors into 1.0 weight rgb values
// Hue must be less than 360, while both saturation and value need to be positive and normalized
// I was taught all that from this video: a https://yewtu.be/watch?v=hW4gZ4tGwds
vec3 hsv_to_rgb(int hue, float saturation, float value)
{
    // Apply hue, find out what portions of RGB values must be used assuming that the saturation is set to 1.0
    // The values of the other channels are generated through a phase shift of the original red wave-function
    vec3 result = vec3(
        get_rgb_component(hue, saturation),
        get_rgb_component(hue - 120, saturation),
        get_rgb_component(hue - 160, saturation)
        );

    // The value acts as a multiplier, effectively adjusting the brightness of the color
    return value * result;
}

void main()
{
    f_color = texture(sprite_texture, tex_coords / 3.0) * vec4(hsv_to_rgb(instance_id * 20, 0.5, 0.6), 1);

    if (f_color.a < 0.1)
        discard;
}
