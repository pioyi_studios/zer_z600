#version 430

layout (location = 0) in vec4 v_position;
layout (location = 1) in vec2 v_tex_coords;
layout (location = 2) in float v_radius;
layout (location = 3) in vec2 v_offset;

// Forward into the fragment shader
out vec2 tex_coords;
flat out int instance_id;

uniform mat4 view_matrix;

void main()
{
    mat4 internal_matrix = mat4(
        v_radius, 0, 0, 0,
	0, v_radius, 0, 0,
	0, 0, v_radius, 0,
	0, 0, 0, 1
    );
    
    gl_Position = view_matrix * (vec4(v_offset, 0, 0) + internal_matrix * v_position);
    tex_coords = v_tex_coords;
    instance_id = gl_InstanceID;
}
