// Adapted by Petros Katiforis from the official site's reference en-/decoder
// I've only made some representation adjustments, it's pretty much the same

#ifndef _QOI_IMAGE_H
#define _QOI_IMAGE_H

#include <stdint.h>
#include <stdbool.h>

typedef struct
{
    uint32_t width;
    uint32_t height;

    // The total amount of components used to represent each pixel
    // RGB has got 3 while RGBA has got an additional one
    uint8_t channels;
    // The color space is purely informative, it does not affect the actual data
    uint8_t colorspace;
    
    uint8_t *data;
} qoi_image_t;

// Reads QOI image into memory and stores data onto the provided image pointer
// Returns zero on success
int qoi_image_decode(qoi_image_t *image, const char *file_path);

int qoi_image_encode(uint8_t *data, uint32_t width, uint32_t height, uint8_t channels, const char *file_path);

#endif
