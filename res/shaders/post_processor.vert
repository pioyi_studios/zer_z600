#version 430

layout (location = 0) in vec4 v_position;
layout (location = 1) in vec2 v_tex_coords;

// Forward into the fragment shader
out vec2 tex_coords;

void main()
{
    gl_Position = v_position;
    tex_coords = v_tex_coords;
}
