/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _PI_TIMERS_H
#define _PI_TIMERS_H

#include "data_structures/linked_list.h"

typedef void (*pi_timer_callback_t) (void *data);

typedef struct pi_timer_t
{
    pi_timer_callback_t callback;
    float remaining_s;
    
    // Custom heap-allocated data
    void *data;
} pi_timer_t;

typedef struct
{
    // They should all be updated after each frame
    pi_linked_list_t active_timers;
} pi_timer_manager_t;

// TODO: Allow for the insertion of regular intervals!

void pi_timer_manager_register(pi_timer_manager_t *m, float seonds, pi_timer_callback_t callback, void *data);
void pi_timer_manager_update(pi_timer_manager_t *m, float delta_s);

#endif
