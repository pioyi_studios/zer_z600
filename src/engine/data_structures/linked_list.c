/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "linked_list.h"
#include <stdlib.h>

void pi_linked_list_insert(pi_linked_list_t *list, void *data)
{
    pi_linked_node_t *node = malloc(sizeof(pi_linked_node_t));
    node->data = data;

    if (*list == NULL)
    {
        *list = node;
    }
    else
    {
        // Insert at the start of the list
        pi_linked_node_t *cdr = *list;
        node->next = cdr;
        *list = node;
    }
}

void pi_linked_list_delete(pi_linked_list_t *list, void *data)
{
    // First, find the node we are interested in
    // Keep track of the previous element so the change can be made
    pi_linked_node_t *prev = NULL;
    pi_linked_node_t *cur = *list;
    
    while (cur != NULL && cur->data != data)
    {
        prev = cur;
        cur = cur->next;
    }

    // If nothing was found, just ignore the request
    if (cur == NULL)
        return;

    // Otherwise, skip the targeted element
    if (prev)
        prev->next = cur->next;

    // If we just need to delete the root, move the list
    else if (cur->next != NULL)
        *list = cur->next;
    // If the list has only one item, delete it
    else
        *list = NULL;
    
    free(cur);
}
