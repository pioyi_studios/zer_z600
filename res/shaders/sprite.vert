#version 430

layout (location = 0) in vec4 v_position;
layout (location = 1) in vec2 v_tex_coords;

// Forward into the fragment shader
out vec2 tex_coords;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform float rotation;

// These are going to be used to offset the texture
uniform vec4 texture_region;

void main()
{
    // Form the rotation matrix ad hoc
    mat4 rotation_matrix = mat4(
        cos(rotation), -sin(rotation), 0, 0,
        sin(rotation), cos(rotation), 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
        );
    
    gl_Position = view_matrix * model_matrix * rotation_matrix * v_position;
    tex_coords = v_tex_coords;
    
    tex_coords.x = tex_coords.x * texture_region.z + texture_region.x;
    tex_coords.y = tex_coords.y * texture_region.w + texture_region.y;
}
