/* Zer Z600
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _ASSETS_H
#define _ASSETS_H

#include "engine/textures.h"
#include "engine/audio.h"
#include "engine/shaders.h"
#include "engine/fonts.h"

/*
 * A place to define and store static assets, such as shaders, fonts, textures and audio
 * These resources will then be indexed by their enum value, which is much faster than using a hashmap
 */

enum {
    SHADER_SPRITE,
    SHADER_PARTICLES,
    SHADER_PLANETS,
    SHADER_DEATH,
    SHADER_SHOCKWAVE,
    SHADER_FADE_PANEL,
    SHADER_TRANSITION,
    SHADER_FUEL_BAR,
    TOTAL_SHADERS
};

enum
{
    AUDIO_COMPLETED,
    AUDIO_FUEL,
    AUDIO_GAME_OVER,
    AUDIO_CRASH,
    TOTAL_AUDIO
};

enum
{
    TEXTURE_ATLAS,
    TEXTURE_ADONIS,
    TEXTURE_LOGO,
    TOTAL_TEXTURES
};

// Define some static arrays to hold the data
extern pi_audio_t sound_effects[TOTAL_AUDIO];
extern pi_texture_t textures[TOTAL_TEXTURES];
extern pi_program_t shaders[TOTAL_SHADERS];
extern pi_font_t main_font;

void load_all_assets(void);

typedef enum
{
    EXPRESSION_HAPPY,
    EXPRESSION_SAD,
    EXPRESSION_ANGRY
} adonis_expression_e;

typedef struct
{
    adonis_expression_e expression;
    char *text;
} adonis_dialog_t;

enum
{
    DIALOG_INTRODUCTION,
    DIALOG_FIRST_FUEL,
    DIALOG_FIRST_CRASH,
    DIALOG_FIRST_ORDER,
    DIALOG_GAME_OVER_POSITIVE,
    DIALOG_GAME_OVER_NEGATIVE,
    DIALOG_RANDOM
};

#define TOTAL_RANDOM 4
#define RANDOM_DIALOG_CHANCE 20

extern adonis_dialog_t dialogs[DIALOG_RANDOM + TOTAL_RANDOM];
extern const char *introduction;

#endif
