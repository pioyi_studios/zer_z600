/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "camera.h"
#include <string.h>

void pi_camera_create(pi_camera_t *camera, float resolution_x, float resolution_y)
{
    // Convert all world coordinates into NDC
    // We will first be scaling down to the correct range and then displace everything
    // Specifically, (0.0f, 0.0f) must actually reside in (-1.0f, 1.0f)
    memset(&camera->world_transform, 0, sizeof(pi_mat4_t));
    memset(&camera->ui_transform, 0, sizeof(pi_mat4_t));
    
    camera->world_transform.matrix[0][0] = camera->ui_transform.matrix[0][0] = 2 / resolution_x;
    camera->world_transform.matrix[1][1] = camera->ui_transform.matrix[1][1] = -2 / resolution_y;
    camera->world_transform.matrix[2][2] = camera->ui_transform.matrix[3][3] = 1.0f;

    camera->ui_transform.matrix[3][0] = -1.0f;
    camera->ui_transform.matrix[3][1] = 1.0f;
    
    pi_camera_set_position(camera, 0.0f, 0.0f);
    camera->world_transform.matrix[3][3] = 1.0f;
}

void pi_camera_set_position(pi_camera_t *camera, float new_x, float new_y)
{
    // Adjust the displacement to the screen's resolution
    camera->world_transform.matrix[3][0] = -1.0f - new_x * camera->world_transform.matrix[0][0];
    camera->world_transform.matrix[3][1] = 1.0f - new_y * camera->world_transform.matrix[1][1];
}

float pi_camera_get_x(pi_camera_t *camera)
{
    // Just perform the reverse operation described by the set_position function
    return (-1.0f - TRANSFORM_GET_X(camera->world_transform)) / camera->world_transform.matrix[0][0];
}

float pi_camera_get_y(pi_camera_t *camera)
{
    return (1.0f - TRANSFORM_GET_Y(camera->world_transform)) / camera->world_transform.matrix[1][1];
}
