#version 430

layout (location = 0) out vec4 f_color;

in vec2 tex_coords;
uniform float transparency;

void main()
{
    f_color = vec4(0, 0, 0, tex_coords.y - transparency);
}
