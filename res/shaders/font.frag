#version 430

layout (location = 0) out vec4 f_color;

uniform sampler2DArray font_texture;
in flat int f_texture_id;
in vec2 f_tex_coords;

void main()
{
    f_color = vec4(1.0, 1.0, 1.0, texture(font_texture, vec3(f_tex_coords.xy, f_texture_id)).r);
}