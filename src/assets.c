/* Zer Z600
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "assets.h"

// Initialize the resources
pi_audio_t sound_effects[TOTAL_AUDIO];
pi_texture_t textures[TOTAL_TEXTURES];
pi_program_t shaders[TOTAL_SHADERS];
pi_font_t main_font;

void load_all_assets(void)
{
    // All the sprites will derive from that single texture
    pi_texture_load_qoi(textures + TEXTURE_ATLAS, "res/texture_atlas.qoi");
    pi_texture_load_qoi(textures + TEXTURE_ADONIS, "res/adonis.qoi");
    pi_texture_load_qoi(textures + TEXTURE_LOGO, "res/logo.qoi");
    
    PI_SHADER_CREATE_PAIR(shaders + SHADER_SPRITE, "res/shaders/sprite.vert", "res/shaders/sprite.frag");
    PI_SHADER_CREATE_PAIR(shaders + SHADER_PARTICLES, "res/shaders/particles.vert", "res/shaders/particles.frag");
    PI_SHADER_CREATE_PAIR(shaders + SHADER_PLANETS, "res/shaders/planets.vert", "res/shaders/planets.frag");
    PI_SHADER_CREATE_PAIR(shaders + SHADER_DEATH, "res/shaders/post_processor.vert", "res/shaders/death.frag");
    PI_SHADER_CREATE_PAIR(shaders + SHADER_SHOCKWAVE, "res/shaders/post_processor.vert", "res/shaders/shockwave.frag");
    PI_SHADER_CREATE_PAIR(shaders + SHADER_FADE_PANEL, "res/shaders/rect.vert", "res/shaders/fade_panel.frag");
    PI_SHADER_CREATE_PAIR(shaders + SHADER_TRANSITION, "res/shaders/post_processor.vert", "res/shaders/transition.frag");
    PI_SHADER_CREATE_PAIR(shaders + SHADER_FUEL_BAR, "res/shaders/rect.vert", "res/shaders/fuel_bar.frag");

    // Load all audio files
    sound_effects[AUDIO_COMPLETED] = pi_audio_load("res/completed.wav");
    sound_effects[AUDIO_FUEL] = pi_audio_load("res/fuel.wav");
    sound_effects[AUDIO_GAME_OVER] = pi_audio_load("res/game_over.wav");
    sound_effects[AUDIO_CRASH] = pi_audio_load("res/explosion.wav");

    pi_font_load_ttf(&main_font, "res/other.ttf");
}

adonis_dialog_t dialogs[] = {
    [DIALOG_INTRODUCTION] = { EXPRESSION_HAPPY, "I always had to make my shit cracker, but not for much longer. This is my chance to bring my publishing house back to life. Pale I cut it, but it's worth a try!" },
    [DIALOG_FIRST_FUEL] = { EXPRESSION_HAPPY, "I'm asswidth! I can't afford any gas, so I'd better collect these suspicious fuel tanks on my way to work!" },
    [DIALOG_FIRST_CRASH] = { EXPRESSION_SAD, "Are you working me? At least I am alive. The garage demanded some cash, but I'm back to business nonetheless" },
    [DIALOG_FIRST_ORDER] = { EXPRESSION_HAPPY, "Look to see! These aliens are pretty generous, they even left me a tip. Never of the drinks would I have imagined that my books were of such interest!" },
    [DIALOG_GAME_OVER_POSITIVE] = { EXPRESSION_SAD, "I'm out of fuel. It's time for me to go home and count my earnings. At least I made some profit! "},
    [DIALOG_GAME_OVER_NEGATIVE] = { EXPRESSION_SAD, "I painted her. Not only have I ran out of fuel, but I'm officially in debt! What a failure! "},
    [DIALOG_RANDOM] = { EXPRESSION_ANGRY, "They exploded nose the cops! I must go to my next order before they find out that I haven't paid the new space-travelling tax!" },
    [DIALOG_RANDOM + 1] = { EXPRESSION_HAPPY, "Life and chicken! I'm making enough money to retire early. I'm hitting myself, but I'm definitely creating a fortune!" },
    [DIALOG_RANDOM + 2] = { EXPRESSION_SAD, "Are they with their goods? These aliens paid only half the price, but I have no chance against them. I stayed bone, I'd better move on" },
    [DIALOG_RANDOM + 3] = { EXPRESSION_SAD, "This place is at devil's mother. I'm so far away from home. I wonder what's going on over there without me.... I hope I don't make them sea, I will end up homeless when I return!" },
};

const char *introduction = "Agapito moy diary, simera einai sabbato kai anakoinose o Adonis, o proin prothypoyrgos mas, oti anoigei space eteria. "
    "Like. . . ayto einai polyyyy cool xD. prepei na einai poly brave gia na parei ena tetoio decision. Tespa, eixame mathima glossas simera kai barethika tin zoi moy. . .  "
    "giati na exoyme tonoys? liges glosses exoyn simera kai den einai KAN gnostes. krima poy o Adonis den kserei agglika, ton koroideyoyn sixna gia ayto kai me, pos to lene, infuriate me. "
    "Tora poy tha paei sto diastima omos everyone tha ton ektimisoyn. Ti biblia na toys kanei sell arage? Kseroyn oi aliens thn alithia gia toys Eyraioys? Prepei kai aytoi na tin mathoyn!1! "
    "Aytaaa xD makari na hmoyn kai ago sto diastima san kai ayton alla exo project na kano finish. Tha niotho perifani kathe fora poy tha blepo, like, traces toy fthinoy toy diastimoplioy ston oyrano! ";
