/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "particle_emitter.h"
#include "sprite.h"
#include <string.h>

void pi_emitter_create(pi_emitter_t *emitter, pi_emitter_settings_t settings)
{
    emitter->settings = settings;
    emitter->active_particles = 0;
    
    // First, we need to allocate enough space to hold the particles' state on the CPU
    emitter->particles = malloc(settings.max_particles * sizeof(pi_particle_t));
    emitter->particle_velocities = malloc(settings.max_particles * sizeof(pi_vec2_t));
    
    glGenVertexArrays(1, &emitter->internal_vao);
    glBindVertexArray(emitter->internal_vao);

    // Pass in the attributes in the correct format
    // First, I am going to provide the common quad
    glBindBuffer(GL_ARRAY_BUFFER, pi_root_vbo);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*) 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*) (2 * sizeof(GLfloat)));

    // Allocate enough GPU memory for the buffer object
    glGenBuffers(1, &emitter->internal_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, emitter->internal_vbo);
    // The data is going to be updated frequently, so I will be using the streaming option
    glBufferData(GL_ARRAY_BUFFER, settings.max_particles * sizeof(pi_particle_t), NULL, GL_DYNAMIC_DRAW);

    // Then, I need to pass all individual particle data
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 0, (GLvoid*) 0);

    // Always use the same quad vertices and texture coordinates
    glVertexAttribDivisor(0, 0);
    glVertexAttribDivisor(1, 0);
    // Split particle data
    glVertexAttribDivisor(2, 1);

    // Calculate the particle's scale
    emitter->scale.x = emitter->settings.scale * emitter->settings.texture_region.w;
    emitter->scale.y = emitter->settings.scale * emitter->settings.texture_region.h;
}

static void respawn_particle(pi_emitter_t *emitter, int index)
{
    memcpy(&emitter->particles[index].position, &emitter->position, sizeof(pi_vec2_t));
    emitter->particles[index].lifetime = emitter->settings.lifetime + emitter->settings.lifetime * (rand() % 50) / 100.0f;
    
    // Add some new, random velocity
    emitter->particle_velocities[index].x = emitter->settings.velocity.x + rand() % 100 - 50;
    emitter->particle_velocities[index].y = emitter->settings.velocity.y + rand() % 100 - 50;
}

void pi_emitter_spawn(pi_emitter_t *emitter)
{
    // If we've already reached the maximum, ignore the request
    if (emitter->active_particles == emitter->settings.max_particles)
        return;
    
    emitter->active_particles++;
    respawn_particle(emitter, emitter->active_particles - 1);
}

static void remove_particle(pi_emitter_t *emitter, int index)
{
    // Replace that particle with the last item on the array
    memcpy(&emitter->particles[index],
           &emitter->particles[emitter->active_particles - 1], sizeof(pi_particle_t));
    
    memcpy(&emitter->particle_velocities[index],
           &emitter->particle_velocities[emitter->active_particles - 1], sizeof(pi_vec2_t));

    emitter->active_particles--;
}

void pi_emitter_update_and_render(pi_emitter_t *emitter, float delta_s,
                                  pi_transform_t *view_matrix, pi_program_t *shader)
{
    if (emitter->active_particles == 0)
        return;
    
    // Future Note: Maybe implement this on a compute shader!
    // I don't need this information on the CPU!
    for (int i = 0; i < emitter->active_particles; i++)
    {
        pi_vec2_t displacement;
        pi_vec2_multiply_by_scalar(&displacement, &emitter->particle_velocities[i], delta_s);
        pi_vec2_add(&emitter->particles[i].position, &emitter->particles[i].position, &displacement);
        emitter->particles[i].lifetime -= delta_s;

        // Check if the particle needs to be respawned
        if (emitter->particles[i].lifetime < 0)
            remove_particle(emitter, i);
    }

    // Send the changes over to the GPU
    glBindBuffer(GL_ARRAY_BUFFER, emitter->internal_vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, emitter->active_particles * sizeof(pi_particle_t), emitter->particles);

    // Render the particles!
    glUseProgram(shader->id);
    glBindTexture(GL_TEXTURE_2D, emitter->settings.texture->id);
    glBindVertexArray(emitter->internal_vao);

    // Calculate the actual texture region in NDC coordinates
    // That way, I can only use a single VBO for all single 2D quads
    float ndc_region[4] = {
        emitter->settings.texture_region.x / (float) emitter->settings.texture->width,
        emitter->settings.texture_region.y / (float) emitter->settings.texture->height,
        emitter->settings.texture_region.w / (float) emitter->settings.texture->width,
        emitter->settings.texture_region.h / (float) emitter->settings.texture->height,
    };

    glUniformMatrix4fv(PI_SHADER_UNIFORM(shader, "view_matrix"), 1, GL_FALSE, (GLfloat*) view_matrix->matrix);
    glUniform4fv(PI_SHADER_UNIFORM(shader, "texture_region"), 1, ndc_region);
    glUniform2fv(PI_SHADER_UNIFORM(shader, "scale"), 1, (GLfloat*) &emitter->scale);
        
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, emitter->active_particles);
}
