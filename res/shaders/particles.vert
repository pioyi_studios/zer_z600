#version 430

layout (location = 0) in vec4 v_position;
layout (location = 1) in vec2 v_tex_coords;
layout (location = 2) in vec4 v_particle_data;

// Forward into the fragment shader
out vec2 f_tex_coords;
out float f_lifetime;

uniform mat4 view_matrix;
uniform vec4 texture_region;
uniform vec2 scale;

void main()
{
    float rotation = v_particle_data.x;
    float lifetime = v_particle_data.y;
    vec2 position = v_particle_data.zw;
    
    gl_Position = view_matrix * vec4(scale * v_position.xy + position.xy, 0, 1);
    f_lifetime = lifetime;

    f_tex_coords = v_tex_coords;
    f_tex_coords.x = f_tex_coords.x * texture_region.z + texture_region.x;
    f_tex_coords.y = f_tex_coords.y * texture_region.w + texture_region.y;
}
