/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "audio.h"
#include <stdlib.h>
#include <string.h>
#include "common.h"

pi_audio_t pi_audio_load(const char *file_path)
{
    Mix_Chunk *audio = Mix_LoadWAV(file_path);

    if (audio == NULL)
        pi_log_fatal("failed to load audio file %s", file_path);

    return audio;
}

void pi_audio_play(pi_audio_t audio)
{
    Mix_PlayChannel(-1, audio, 0);
}
