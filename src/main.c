/* Zer Z600
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <GL/glew.h>
#include <math.h>
#include <GLFW/glfw3.h>
#include "engine/common.h"
#include "engine/data_structures/dyn_array.h"
#include "engine/animation_manager.h"
#include "engine/timers.h"
#include "engine/particle_emitter.h"
#include "engine/post_processor.h"
#include "engine/app.h"
#include "engine/sprite.h"
#include "engine/camera.h"
#include "assets.h"

#define WINDOW_WIDTH 860
#define WINDOW_HEIGHT 860
#define DELTA_TIME 1.0 / 60
#define PLANETS_PER_CHUNK 15
#define SPACESHIP_POWER 700
#define GRAVITATIONAL_PULL 7
#define CHUNK_SIZE 2000
#define CHUNK_ARRAY_DIM 15
#define PIXELS_PER_DOLLAR 20
#define COST_OF_REPAIR 100
#define TRANSITION_SPEED 1.0f
#define ADONIS_ANIMATION_SPEED 0.6f
#define STARTING_FUEL 2000
#define FUEL_PER_SECOND 30
#define FUEL_PER_TANK 240

typedef enum {
    SCENE_LOGO,
    SCENE_INTRODUCTION,
    SCENE_SPACE,
    TOTAL_SCENES
} scene_e;

typedef void (*scene_load_handler) (void);
typedef void (*scene_render_handler) (void);

typedef struct
{
    scene_load_handler loader;
    scene_render_handler renderer;
    // I might add some more data here on the future
} scene_t;

typedef struct
{
    float radius;
    pi_vec2_t position;
} planet_t;

/*
 * The universe will be split in chunks, just like in real life!1!!1
 * Each chunk will contain a limited number of planets and I'm only going
 * to be processing one at a time. Collisions and gravity forces will be optimized
 */
typedef struct
{
    planet_t planets[PLANETS_PER_CHUNK];
    GLint instancing_vao;

    // Leave to zero to denote that no fuel tank exists
    pi_vec2_t fuel_position;
} universe_chunk_t;

struct
{
    pi_animation_t *active_adonis;
    
    // Some state tracking variables
    bool has_ever_collected_fuel;
    bool has_ever_crashed;
    bool has_ever_completed_order;
    bool is_game_over;

    scene_t scenes[TOTAL_SCENES];
    scene_e active_scene;
    GLFWwindow *window;
    pi_camera_t camera;
    pi_anim_manager_t animations;
    pi_timer_manager_t timers;
    pi_post_processor_t processor;
    bool is_paused;

    pi_text_t introductory_text;
    pi_sprite_t van;
    pi_vec2_t van_velocity;
    pi_emitter_t van_smoke;
    pi_sprite_t location_indicator;
    planet_t *next_planet;
    int next_planet_prize;
    // The universe will just be a 2D array of chunks
    universe_chunk_t chunks[CHUNK_ARRAY_DIM][CHUNK_ARRAY_DIM];

    // A base sprite that is going to be moved around to render all fuel tanks
    // Most of the time, only one will actually be visible at any given time
    // That way, I don't have to store a whole matrix for each instance
    pi_sprite_t base_fuel_sprite;
    pi_sprite_t success_indicator;

    // Adonis dialog box
    pi_sprite_t adonis;
    pi_sprite_t fade_panel;
    pi_text_t adonis_text;

    int balance;
    pi_text_t balance_text;
    pi_sprite_t logo;
    pi_text_t logo_text;
    pi_sprite_t balance_icon;
    pi_sprite_t fuel_collected;
    pi_sprite_t fuel_icon;
    pi_sprite_t fuel_bar;
    float fuel;
} ctx;

static bool is_valid_chunk(int chunk_x, int chunk_y)
{
    return chunk_x >= 0 && chunk_x <= CHUNK_ARRAY_DIM - 1 &&
        chunk_y >= 0 && chunk_y <= CHUNK_ARRAY_DIM - 1;
}

static void select_next_planet(void)
{
    int rand_x, rand_y;
    int current_x = TRANSFORM_GET_X(ctx.van.transform) / CHUNK_SIZE;
    int current_y = TRANSFORM_GET_Y(ctx.van.transform) / CHUNK_SIZE;
    int distance = rand() % 8 - 4;
    
    // Choose a random chunk from a random distance away
    do
    {
        rand_x = current_x + distance;
        rand_y = current_y + distance;

        // Bring closer to zero
        // The loop will finish once it reached the current chunk
        distance += (distance < 0 ? 1 : -1);
    } while (!is_valid_chunk(rand_x, rand_y));

    ctx.next_planet = &ctx.chunks[rand_y][rand_x].planets[rand() % PLANETS_PER_CHUNK];

    // Select a prize based on the distance
    ctx.next_planet_prize = pi_sprite_get_distance(&ctx.van, ctx.next_planet->position.x, ctx.next_planet->position.y) / PIXELS_PER_DOLLAR;
}

static void center_camera_on_sprite(pi_sprite_t *sprite)
{
    pi_camera_set_position(&ctx.camera,
                           TRANSFORM_GET_X(sprite->transform) - WINDOW_WIDTH / 2.0f,
                           TRANSFORM_GET_Y(sprite->transform) - WINDOW_HEIGHT / 2.0f);
}

static void update_balance(int new_value)
{
    // First, convert it to a string
    char string[20];
    sprintf(string, "%d", new_value);
    pi_text_set_content(&ctx.balance_text, &main_font, string, 22.0f, -1);

    ctx.balance = new_value;
}

static void readjust_fuel_bar(void)
{
    pi_sprite_set_scale(&ctx.fuel_bar, 1.0f, ctx.fuel / STARTING_FUEL);
    TRANSFORM_SET_Y(ctx.fuel_bar.transform, 40 + 7 * (1.0f - ctx.fuel / STARTING_FUEL));
}

static void spawn_fuel_tank_based_on_chunk(int x, int y)
{
    // If not fuel has spawned yet, replace it
    if (is_valid_chunk(x, y))
    {
        universe_chunk_t *chunk = &ctx.chunks[y][x];

        for (;;)
        {
            chunk->fuel_position.x = x * CHUNK_SIZE + 30 + rand() % (CHUNK_SIZE - 60);
            chunk->fuel_position.y = y * CHUNK_SIZE + 30 + rand() % (CHUNK_SIZE - 60);

            // If it is colliding with a planet, attempt again
            bool is_colliding = false;
            pi_vec2_t distance;
            
            for (int i = 0; i < PLANETS_PER_CHUNK; i++)
            {
                distance.x = chunk->fuel_position.x - chunk->planets[i].position.x;
                distance.y = chunk->fuel_position.y - chunk->planets[i].position.y;

                if (pi_vec2_get_magnitude(&distance) < chunk->planets[i].radius + 50)
                {
                    is_colliding = true;
                    break;
                }
            }

            if (!is_colliding)
                return;
        }
    }
}

static void adonis_dialog_animation(pi_animation_t *anim, float time_progress, void *data)
{
    // Check if the animation has finished
    if (anim->is_reversed && time_progress == 0.0f)
    {
        ctx.active_adonis = NULL;
    }
    else
    {
        // Move everything on top, with a constant speed
        ctx.adonis_text.position.y = (1.0f - time_progress) * (WINDOW_HEIGHT + 20) + time_progress * (WINDOW_HEIGHT - ctx.adonis_text.height - 10);

        // Just a basic linear interpolation.
        TRANSFORM_SET_Y(ctx.adonis.transform, (1 - time_progress) * (WINDOW_HEIGHT + TRANSFORM_GET_HEIGHT(ctx.adonis.transform) + 10) +
                        time_progress * (WINDOW_HEIGHT - TRANSFORM_GET_HEIGHT(ctx.adonis.transform)));

        TRANSFORM_SET_Y(ctx.fade_panel.transform, (1 - time_progress) * (WINDOW_HEIGHT + TRANSFORM_GET_HEIGHT(ctx.fade_panel.transform)) +
                        time_progress * (WINDOW_HEIGHT - TRANSFORM_GET_HEIGHT(ctx.fade_panel.transform)));
    }
}

static void play_adonis_dialog(int dialog_index)
{
    // If an animation is already taking place, delete that
    if (ctx.active_adonis != NULL)
    {
        pi_anim_manager_delete(&ctx.animations, ctx.active_adonis);
    }

    const int padding = 130;

    // Position everything at the bottom of the screen
    ctx.adonis_text.position.x = padding;
    ctx.adonis_text.position.y = WINDOW_HEIGHT + 10;
    TRANSFORM_SET_X(ctx.fade_panel.transform, WINDOW_WIDTH / 2.0f);
    TRANSFORM_SET_Y(ctx.fade_panel.transform, TRANSFORM_GET_HEIGHT(ctx.fade_panel.transform));
    TRANSFORM_SET_X(ctx.adonis.transform, WINDOW_WIDTH - TRANSFORM_GET_WIDTH(ctx.adonis.transform) - padding);
    TRANSFORM_SET_Y(ctx.adonis.transform, WINDOW_HEIGHT + TRANSFORM_GET_HEIGHT(ctx.adonis.transform) + 10);

    // Change the expression based on the dialog data
    ctx.adonis.texture_region.x = ctx.adonis.texture_region.w * dialogs[dialog_index].expression;
    
    ctx.is_paused = true;
    pi_text_set_content(&ctx.adonis_text, &main_font, dialogs[dialog_index].text, 22.0f, 280.0f);
    ctx.active_adonis = pi_anim_manager_trigger(&ctx.animations, adonis_dialog_animation, ADONIS_ANIMATION_SPEED, PI_INTERPOLATION_LINEAR, NULL, false);
}

static void game_over_animation(pi_animation_t *anim, float time_progress, void *data)
{
    if (time_progress == 0.0f)
    {
        ctx.is_paused = true;
        ctx.is_game_over = true;
        pi_audio_play(sound_effects[AUDIO_GAME_OVER]);
        play_adonis_dialog(ctx.balance > 0 ? DIALOG_GAME_OVER_POSITIVE : DIALOG_GAME_OVER_NEGATIVE);
    }
    else
    {
        // Just slowing hide everything, and move the text to the center of the screen
        ctx.fuel_bar.transparency = time_progress;
        ctx.fuel_icon.transparency = time_progress;
        
        TRANSFORM_SET_X(ctx.balance_icon.transform, time_progress * WINDOW_WIDTH / 2.0f - 13);
        ctx.balance_text.position.x = time_progress * WINDOW_WIDTH / 2.0f;
    }
}

static void scene_transition_animation(pi_animation_t *anim, float time_progress, void *data)
{
    scene_e next_scene = *(scene_e*) data;
    const float dark_delay = 0.2f;
    static bool has_reached_black;

    glUseProgram(shaders[SHADER_TRANSITION].id);
    
    if (time_progress == 0.0f)
    {
        // Activate the fading post processing effect
        pi_post_processor_activate(&ctx.processor, shaders + SHADER_TRANSITION);
        has_reached_black = false;
    }
    else if (time_progress == 1.0f)
    {
        // If the animation has finished, clear the data
        free(data);
        pi_post_processor_disable(&ctx.processor);
        has_reached_black = false;
    }
    else if (time_progress < 0.5f - dark_delay)
    {
        // Fade in
        glUniform1f(PI_SHADER_UNIFORM(shaders + SHADER_TRANSITION, "darkness"), time_progress / (0.5f - dark_delay));
    }
    else if (time_progress > 0.5f + dark_delay)
    {
        glUniform1f(PI_SHADER_UNIFORM(shaders + SHADER_TRANSITION, "darkness"), 1.0f - (time_progress - (0.5f + dark_delay)) / (0.5f - dark_delay));
    }
    // If we're inside the delay region, check if this is the first time going there
    else if (!has_reached_black)
    {
        ctx.active_scene = next_scene;
        has_reached_black = true;
        glUniform1f(PI_SHADER_UNIFORM(shaders + SHADER_TRANSITION, "darkness"), 1.0f);

        // Call the scene's load functionas automaticamentos!
        ctx.scenes[ctx.active_scene].loader();
    }
}

static void death_animation(pi_animation_t *anim, float time_progress, void *data)
{
    static bool has_moved_stuff = false;
    
    // If the animation has just started, pause the game first
    if (time_progress == 0.0f)
    {
        pi_post_processor_activate(&ctx.processor, shaders + SHADER_DEATH);
        pi_audio_play(sound_effects[AUDIO_CRASH]);
        update_balance(ctx.balance - COST_OF_REPAIR);
        ctx.is_paused = true;
    }
    else if (time_progress != 1.0f)
    {
        glUseProgram(ctx.processor.shader->id);
        
        // Perform a simple transition using an after-effect
        if (time_progress < 0.3f || time_progress > 0.7f)
        {
            float darkness = (time_progress < 0.3f ? time_progress / 0.3f : 1.0f - (time_progress - 0.7f) / 0.3f);
            glUniform1f(PI_SHADER_UNIFORM(ctx.processor.shader, "darkness"), darkness);
        }
        else if (!has_moved_stuff)
        {
            // The screen is now black, move the player inside
            // Spawn at the beginning of the current chunk
            TRANSFORM_SET_X(ctx.van.transform, (int) TRANSFORM_GET_X(ctx.van.transform) / CHUNK_SIZE * CHUNK_SIZE);
            TRANSFORM_SET_Y(ctx.van.transform, (int) TRANSFORM_GET_Y(ctx.van.transform) / CHUNK_SIZE * CHUNK_SIZE);
            center_camera_on_sprite(&ctx.van);

            has_moved_stuff = true;
        }
    }
    else
    {
        // We've reached the end of the animation, let the player move again
        pi_post_processor_disable(&ctx.processor);
        // Prepare for next trigger
        has_moved_stuff = false;
        ctx.is_paused = false;

        // Check if it the first time completing an order
        if (!ctx.has_ever_crashed)
        {
            ctx.has_ever_crashed = true;
            play_adonis_dialog(DIALOG_FIRST_CRASH);
        }

        memset(&ctx.van_velocity, 0, sizeof(pi_vec2_t));
    }
}

static void completed_animation(pi_animation_t *anim, float time_progress, void *data)
{
    static pi_vec2_t planet_position;
    
    if (time_progress == 0.0f)
    {
        pi_audio_play(sound_effects[AUDIO_COMPLETED]);
        pi_post_processor_activate(&ctx.processor, shaders + SHADER_SHOCKWAVE);
        
        planet_position = ctx.next_planet->position;
        planet_position.y -= ctx.next_planet->radius;
        TRANSFORM_SET_X(ctx.success_indicator.transform, planet_position.x);
        update_balance(ctx.balance + ctx.next_planet_prize);

        // Check if it the first time completing an order
        if (!ctx.has_ever_completed_order)
        {
            ctx.has_ever_completed_order = true;
            play_adonis_dialog(DIALOG_FIRST_ORDER);
        }
        else
        {
            // There's a small chance than a random dialog might pop up
            if (rand() % RANDOM_DIALOG_CHANCE == 0)
            {
                play_adonis_dialog(DIALOG_RANDOM + rand() % TOTAL_RANDOM);
            }
        }

        // Select a new planet destination
        select_next_planet();
    }
    else if (time_progress == 1.0f)
    {
        if (ctx.processor.shader == shaders + SHADER_SHOCKWAVE)
            pi_post_processor_disable(&ctx.processor);
    }
    else
    {
        // Let the player know that the delivery was a success
        TRANSFORM_SET_Y(ctx.success_indicator.transform, planet_position.y - time_progress * 50);
        // Update the sprite's transparency as well
        ctx.success_indicator.transparency = time_progress;
        pi_sprite_render_textured(&ctx.success_indicator, &textures[TEXTURE_ATLAS], &ctx.camera.world_transform, shaders + SHADER_SPRITE);

        // Update shockwave progress
        // If the death animation is playing, don't enforce the shockwave
        if (ctx.processor.shader == shaders + SHADER_SHOCKWAVE)
        {
            glUseProgram(shaders[SHADER_SHOCKWAVE].id);
            glUniform1f(PI_SHADER_UNIFORM(shaders + SHADER_SHOCKWAVE, "progress"), time_progress);
        }
    }
}

static void fuel_collected_animation(pi_animation_t *anim, float time_progress, void *data)
{
    if (time_progress == 0.0f)
    {
        int chunk_y = (int) TRANSFORM_GET_Y(ctx.van.transform) / CHUNK_SIZE;
        int chunk_x = (int) TRANSFORM_GET_X(ctx.van.transform) / CHUNK_SIZE;
        
        // Position the indicator right at the location of the current fuel tank
        universe_chunk_t *chunk = &ctx.chunks[chunk_y][chunk_x];
        TRANSFORM_SET_X(ctx.fuel_collected.transform, chunk->fuel_position.x);
        TRANSFORM_SET_Y(ctx.fuel_collected.transform, chunk->fuel_position.y);

        ctx.fuel = MIN(STARTING_FUEL, ctx.fuel + FUEL_PER_TANK);
        pi_audio_play(sound_effects[AUDIO_FUEL]);
        readjust_fuel_bar();
        
        // If this is the first time collecting fuel, pop up a dialog
        if (!ctx.has_ever_collected_fuel)
        {
            ctx.has_ever_collected_fuel = true;
            play_adonis_dialog(DIALOG_FIRST_FUEL);
        }
        
        // Spawn another fuel tank somewhere else
        spawn_fuel_tank_based_on_chunk(chunk_x, chunk_y);
    }
    else
    {
        // Rotate around and scale down!
        pi_sprite_set_scale(&ctx.fuel_collected, 2.0f * (1.0f - time_progress), 2.0f * (1.0f - time_progress));
        ctx.fuel_collected.transform.rotation += 30 * DELTA_TIME;
        ctx.fuel_collected.transparency = time_progress;
        
        // Render onto the screen
        pi_sprite_render_textured(&ctx.fuel_collected, &textures[TEXTURE_ATLAS], &ctx.camera.world_transform, shaders + SHADER_SPRITE);
    }
}

static void create_chunk(int x, int y)
{
    GLint planet_data_vbo;
    universe_chunk_t *chunk = &ctx.chunks[y][x];
    
    // First, before sending the data over to the GPU, spawn the planets
    for (int i = 0; i < PLANETS_PER_CHUNK; i++)
    {
        chunk->planets[i].position.x = 50 + x * CHUNK_SIZE + rand() % (CHUNK_SIZE - 100);
        chunk->planets[i].position.y = 50 + y * CHUNK_SIZE + rand() % (CHUNK_SIZE - 100);

        chunk->planets[i].radius = (rand() % 3 + 1) * 32;
    }

    // Spawn a fuel tank
    spawn_fuel_tank_based_on_chunk(x, y);

    glGenVertexArrays(1, &ctx.chunks[y][x].instancing_vao);
    glBindVertexArray(ctx.chunks[y][x].instancing_vao);

    // Describe the layout of the vertex array object
    // First, pass in the common quad
    glBindBuffer(GL_ARRAY_BUFFER, pi_root_vbo);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*) 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*) (2 * sizeof(GLfloat)));

    // Generate the vertex buffer object that will hold unique values
    glGenBuffers(1, &planet_data_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, planet_data_vbo);

    // The values will not be modified, so I'm marking this as static
    glBufferData(GL_ARRAY_BUFFER, PLANETS_PER_CHUNK * sizeof(planet_t), &ctx.chunks[y][x].planets, GL_STATIC_DRAW);

    // Register the custom fields
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(planet_t), (GLvoid*) 0);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(planet_t), (GLvoid*) (1 * sizeof(GLfloat)));

    // One for each. The rest will be common!
    glVertexAttribDivisor(2, 1);
    glVertexAttribDivisor(3, 1);
}

static void render_chunk(int x, int y)
{
    universe_chunk_t *chunk = &ctx.chunks[y][x];
    
    // Active the shader and pass in the camera's view matrix
    glUseProgram(shaders[SHADER_PLANETS].id);
    glBindVertexArray(chunk->instancing_vao);
    glBindTexture(GL_TEXTURE_2D, textures[TEXTURE_ATLAS].id);
    glUniformMatrix4fv(PI_SHADER_UNIFORM(shaders + SHADER_PLANETS, "view_matrix"),
                       1, GL_FALSE, (GLfloat*) ctx.camera.world_transform.matrix);
        
    // Render all planets with a *single* draw call!
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, PLANETS_PER_CHUNK);

    // Render the fuel tank too
    TRANSFORM_SET_X(ctx.base_fuel_sprite.transform, chunk->fuel_position.x);
    TRANSFORM_SET_Y(ctx.base_fuel_sprite.transform, chunk->fuel_position.y);
    pi_sprite_render_textured(&ctx.base_fuel_sprite, &textures[TEXTURE_ATLAS], &ctx.camera.world_transform, shaders + SHADER_SPRITE);
}

// Calculate and apply the Newtonian forces of the planets towards the spaceship
static void chunk_attract_spaceship(int x, int y)
{
    universe_chunk_t *chunk = &ctx.chunks[y][x];

    for (int i = 0; i < PLANETS_PER_CHUNK; i++)
    {
        planet_t *planet = &chunk->planets[i];
        
        // First, calculate their distance
        pi_vec2_t distance = {
            planet->position.x - TRANSFORM_GET_X(ctx.van.transform),
            planet->position.y - TRANSFORM_GET_Y(ctx.van.transform)
        };

        float distance_mag = pi_vec2_get_magnitude(&distance);

        // Check if a collision has occured while you're at it
        if (distance_mag < planet->radius - 10)
        {
            // Play the death animation
            pi_anim_manager_trigger(&ctx.animations, death_animation, 2.0f, PI_INTERPOLATION_QUADRATIC, NULL, false);
            return;
        }
        
        // Normalize the vector
        pi_vec2_multiply_by_scalar(&distance, &distance, 1 / distance_mag);

        int mass = planet->radius * planet->radius * GRAVITATIONAL_PULL;
        
        // Multiply by the formula's scalars
        // The planet's mass is an arbitrary function of its radius
        pi_vec2_multiply_by_scalar(&distance, &distance,
                                   mass / distance_mag / distance_mag);

        // Apply to velocity
        pi_vec2_add(&ctx.van_velocity, &ctx.van_velocity, &distance);
    }
}

static void update_and_render_chunks(void)
{
    // First, calculate at which chunk the spacship is currently located
    int chunk_x = TRANSFORM_GET_X(ctx.van.transform) / CHUNK_SIZE;
    int chunk_y = TRANSFORM_GET_Y(ctx.van.transform) / CHUNK_SIZE;
    
    pi_vec2i_t active_chunks[4];
    int i = 0;

    active_chunks[i++] = (pi_vec2i_t) {chunk_x, chunk_y};
    
    // Determine if any other chunks are visible as well
    const int extra_spacing = 100;
    bool is_left = (int) TRANSFORM_GET_X(ctx.van.transform) % CHUNK_SIZE < WINDOW_WIDTH / 2.0f + extra_spacing;
    bool is_right = (int) TRANSFORM_GET_X(ctx.van.transform) % CHUNK_SIZE > CHUNK_SIZE - WINDOW_WIDTH / 2.0f - extra_spacing;
    bool is_top = (int) TRANSFORM_GET_Y(ctx.van.transform) % CHUNK_SIZE < WINDOW_HEIGHT / 2.0f + extra_spacing;
    bool is_bottom = (int) TRANSFORM_GET_Y(ctx.van.transform) % CHUNK_SIZE > CHUNK_SIZE - WINDOW_HEIGHT / 2.0f - extra_spacing;

    if (is_top) active_chunks[i++] = (pi_vec2i_t) {chunk_x, chunk_y - 1};
    else if (is_bottom) active_chunks[i++] = (pi_vec2i_t) {chunk_x, chunk_y + 1};

    if (is_left)
    {
        active_chunks[i++] = (pi_vec2i_t) {chunk_x - 1, chunk_y};

        // If it should go on top as well, append the top left corner
        if (is_top) active_chunks[i++] = (pi_vec2i_t) {chunk_x - 1, chunk_y - 1};
        else if (is_bottom) active_chunks[i++] = (pi_vec2i_t) {chunk_x - 1, chunk_y + 1};
    }
    else if (is_right)
    {
        active_chunks[i++] = (pi_vec2i_t) {chunk_x + 1, chunk_y};

        if (is_top) active_chunks[i++] = (pi_vec2i_t) {chunk_x + 1, chunk_y - 1};
        else if (is_bottom) active_chunks[i++] = (pi_vec2i_t) {chunk_x + 1, chunk_y + 1};
    }
    
    if (is_valid_chunk(chunk_x, chunk_y) && !ctx.is_paused)
    {
        chunk_attract_spaceship(chunk_x, chunk_y);
        universe_chunk_t *chunk = &ctx.chunks[chunk_y][chunk_x];
        
        // Check if the spaceship is currently colliding with a fuel tank
        if (pi_sprite_get_distance(&ctx.van, chunk->fuel_position.x, chunk->fuel_position.y) < 70)
        {
            pi_anim_manager_trigger(&ctx.animations, fuel_collected_animation, 1.0f, PI_INTERPOLATION_QUADRATIC, NULL, false);
        }
    }
    
    // Render all active chunks
    for (int j = 0; j < i; j++)
    {
        if (is_valid_chunk(active_chunks[j].x, active_chunks[j].y))
            render_chunk(active_chunks[j].x, active_chunks[j].y);
    }
}

void scene_transition_timer_callback(void *data)
{
    // Just call the transition animations
    pi_anim_manager_trigger(&ctx.animations, scene_transition_animation, TRANSITION_SPEED, PI_INTERPOLATION_LINEAR, data, false);
}

static void handle_spaceship_input(void)
{
    if (!ctx.is_paused)
    {
        // Implementing some smooth camera movement
        int direction_x = (glfwGetKey(ctx.window, GLFW_KEY_D) == GLFW_PRESS) - (glfwGetKey(ctx.window, GLFW_KEY_A) == GLFW_PRESS);
        int direction_y = (glfwGetKey(ctx.window, GLFW_KEY_S) == GLFW_PRESS) - (glfwGetKey(ctx.window, GLFW_KEY_W) == GLFW_PRESS);

        ctx.van_velocity.x += direction_x * SPACESHIP_POWER * DELTA_TIME;
        ctx.van_velocity.y += direction_y * SPACESHIP_POWER * DELTA_TIME;

        if (ctx.van_velocity.x != 0.0f || ctx.van_velocity.y != 0.0f)
        {
            TRANSFORM_MOVE_X(ctx.van.transform, ctx.van_velocity.x * DELTA_TIME);
            TRANSFORM_MOVE_Y(ctx.van.transform, ctx.van_velocity.y * DELTA_TIME);

            if (direction_x != 0.0f || direction_y != 0.0f)
            {
                // If we're on the space scene and the player is trying to move, consume fuel
                if (ctx.active_scene == SCENE_SPACE)
                {
                    ctx.fuel -= DELTA_TIME * FUEL_PER_SECOND;

                    // If the fuel has gone to zero, start that game over animation!
                    if (ctx.fuel <= 0)
                        pi_anim_manager_trigger(&ctx.animations, game_over_animation, TRANSITION_SPEED, PI_INTERPOLATION_LINEAR, NULL, false);
                    
                    else
                        readjust_fuel_bar();
                }
                
                // Move the particle system along with the spaceship
                ctx.van_smoke.position.x = TRANSFORM_GET_X(ctx.van.transform) + TRANSFORM_GET_WIDTH(ctx.van.transform) / 2.0f - 19;
                ctx.van_smoke.position.y = TRANSFORM_GET_Y(ctx.van.transform) + TRANSFORM_GET_HEIGHT(ctx.van.transform) - 14;

                // If there is some movement, spawn some smoke!
                pi_emitter_spawn(&ctx.van_smoke);
                pi_emitter_spawn(&ctx.van_smoke);
            }
        }
    }
}

static void on_space_scene_render(void)
{ 
    // Update function
    handle_spaceship_input();
    center_camera_on_sprite(&ctx.van);
    
    update_and_render_chunks();
    
    // The smoke should be rendered behind the spaceship
    pi_emitter_update_and_render(&ctx.van_smoke, DELTA_TIME, &ctx.camera.world_transform, shaders + SHADER_PARTICLES);
    pi_sprite_render_textured(&ctx.van, &textures[TEXTURE_ATLAS], &ctx.camera.world_transform, shaders + SHADER_SPRITE);

    // Render the location indicator
    // If it is visible on screen, just render it on top of the planet
    int point_y = ctx.next_planet->position.y - ctx.next_planet->radius - 18;
    if (fabs(ctx.next_planet->position.x - TRANSFORM_GET_X(ctx.van.transform)) < WINDOW_WIDTH / 2 &&
        fabs(point_y - TRANSFORM_GET_Y(ctx.van.transform)) < WINDOW_HEIGHT / 2)
    {
        TRANSFORM_SET_X(ctx.location_indicator.transform, ctx.next_planet->position.x);
        TRANSFORM_SET_Y(ctx.location_indicator.transform, point_y);
        pi_sprite_render_textured(&ctx.location_indicator, &textures[TEXTURE_ATLAS], &ctx.camera.world_transform, shaders + SHADER_SPRITE);
    }
    else
    {
        pi_vec2_t direction = {
            ctx.next_planet->position.y - TRANSFORM_GET_Y(ctx.van.transform),
            ctx.next_planet->position.x - TRANSFORM_GET_X(ctx.van.transform),
        };

        // Normalize and multiply by some radius
        pi_vec2_normalize(&direction, &direction);
        
        pi_vec2_multiply_by_scalar(&direction, &direction, 60);

        TRANSFORM_SET_X(ctx.location_indicator.transform, WINDOW_WIDTH / 2.0f + direction.y);
        TRANSFORM_SET_Y(ctx.location_indicator.transform, WINDOW_HEIGHT / 2.0f + direction.x);

        ctx.location_indicator.transparency = 0.7f;
        pi_sprite_render_textured(&ctx.location_indicator, &textures[TEXTURE_ATLAS], &ctx.camera.ui_transform, shaders + SHADER_SPRITE);
        ctx.location_indicator.transparency = 0.0f;
    }

    if (ctx.active_adonis != NULL)
    {
        pi_sprite_render(&ctx.fade_panel, &ctx.camera.ui_transform, shaders + SHADER_FADE_PANEL);
        pi_text_render(&ctx.adonis_text, &ctx.camera.ui_transform);
        pi_sprite_render_textured(&ctx.adonis, &textures[TEXTURE_ADONIS], &ctx.camera.ui_transform, shaders + SHADER_SPRITE);
    }

    // Render the balance on top of everything
    pi_text_render(&ctx.balance_text, &ctx.camera.ui_transform);
    pi_sprite_render_textured(&ctx.balance_icon, &textures[TEXTURE_ATLAS], &ctx.camera.ui_transform, shaders + SHADER_SPRITE);
    pi_sprite_render_textured(&ctx.fuel_icon, &textures[TEXTURE_ATLAS], &ctx.camera.ui_transform, shaders + SHADER_SPRITE);
    
    glUseProgram(shaders[SHADER_FUEL_BAR].id);
    glUniform1f(PI_SHADER_UNIFORM(shaders + SHADER_FUEL_BAR, "time"), glfwGetTime());
    pi_sprite_render(&ctx.fuel_bar, &ctx.camera.ui_transform, shaders + SHADER_FUEL_BAR);
}

/*
 * The game's introduction will be displayed a teenager's calendar in greeklish
 * The spaceship will be on the background and the player will be able to control it
 */
static void on_introduction_scene_render(void)
{
    handle_spaceship_input();
    
    pi_emitter_update_and_render(&ctx.van_smoke, DELTA_TIME, &ctx.camera.world_transform, shaders + SHADER_PARTICLES);
    pi_sprite_render_textured(&ctx.van, &textures[TEXTURE_ATLAS], &ctx.camera.world_transform, shaders + SHADER_SPRITE);

    pi_text_render(&ctx.introductory_text, &ctx.camera.ui_transform);
}


static void on_logo_scene_render(void)
{
    pi_sprite_render_textured(&ctx.logo, &textures[TEXTURE_LOGO], &ctx.camera.world_transform, shaders + SHADER_SPRITE);
    pi_text_render(&ctx.logo_text, &ctx.camera.ui_transform);
}

void on_logo_scene_load(void)
{
    scene_e *scene = malloc(sizeof(scene_e));
    *scene = SCENE_INTRODUCTION;
    // When the logo scene is loaded, create a timer to trigger a transition
    pi_timer_manager_register(&ctx.timers, 2.0f, scene_transition_timer_callback, scene);
}

void on_introduction_scene_load(void)
{
    // Make it false so that the van can move around
    ctx.is_paused = false;
    memset(&ctx.van_velocity, 0, sizeof(pi_vec2_t));
    
    TRANSFORM_SET_X(ctx.van.transform, WINDOW_WIDTH / 2.0f);
    TRANSFORM_SET_Y(ctx.van.transform, WINDOW_HEIGHT - 100);
    center_camera_on_sprite(&ctx.van);
}

void on_space_scene_load(void)
{
    ctx.is_game_over = false;
    memset(&ctx.van_velocity, 0, sizeof(pi_vec2_t));
    TRANSFORM_SET_X(ctx.van.transform, CHUNK_SIZE * (CHUNK_ARRAY_DIM / 2));
    TRANSFORM_SET_Y(ctx.van.transform, CHUNK_SIZE * (CHUNK_ARRAY_DIM / 2));
    
    ctx.fuel_bar.transparency = 0.0f;
    ctx.fuel_icon.transparency = 0.0f;
    ctx.is_paused = false;
    ctx.active_adonis = NULL;
    ctx.fuel = STARTING_FUEL;
    readjust_fuel_bar();
    update_balance(0);

    // These positions might have changed due to the game over animation
    TRANSFORM_SET_X(ctx.fuel_icon.transform, TRANSFORM_GET_WIDTH(ctx.fuel_icon.transform) + 10);
    TRANSFORM_SET_Y(ctx.fuel_icon.transform, TRANSFORM_GET_HEIGHT(ctx.fuel_icon.transform) + 10);
    TRANSFORM_SET_X(ctx.fuel_bar.transform, 42);
    TRANSFORM_SET_Y(ctx.fuel_bar.transform, 40);

    TRANSFORM_SET_X(ctx.balance_icon.transform, TRANSFORM_GET_X(ctx.fuel_icon.transform) + TRANSFORM_GET_WIDTH(ctx.fuel_icon.transform)
                    + TRANSFORM_GET_WIDTH(ctx.balance_icon.transform) + 13);
    TRANSFORM_SET_Y(ctx.balance_icon.transform, TRANSFORM_GET_HEIGHT(ctx.balance_icon.transform) + 15);
    ctx.balance_text.position.y = TRANSFORM_GET_Y(ctx.balance_icon.transform) + 4;
    ctx.balance_text.position.x = TRANSFORM_GET_X(ctx.balance_icon.transform) + 14;

    select_next_planet();
    play_adonis_dialog(DIALOG_INTRODUCTION);
}

static void mouse_button_callback(GLFWwindow *window, int button, int action, int mods)
{
    // Collect mouse position
    double mouse_x, mouse_y;
    glfwGetCursorPos(window, &mouse_x, &mouse_y);
    
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
    }
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ENTER && action == GLFW_PRESS)
    {
        // If enter is pressed on the introductory scene, go straight to the gameplay
        if (ctx.active_scene == SCENE_INTRODUCTION)
        {
            scene_e *scene = malloc(sizeof(scene_e));
            *scene = SCENE_SPACE;
            // Just call the transition animations
            pi_anim_manager_trigger(&ctx.animations, scene_transition_animation, TRANSITION_SPEED, PI_INTERPOLATION_LINEAR, scene, false);
        }
        // If a dialog is currently active, just skip that
        else if (ctx.is_paused && ctx.active_adonis != NULL)
        {
            if (ctx.is_game_over)
            {
                // Transition back to the game's introduction scene
                scene_e *scene = malloc(sizeof(scene_e));
                *scene = SCENE_INTRODUCTION;
                // Just call the transition animations
                pi_anim_manager_trigger(&ctx.animations, scene_transition_animation, TRANSITION_SPEED, PI_INTERPOLATION_LINEAR, scene, false);
            }
            else
            {
                ctx.is_paused = false;
                ctx.active_adonis = pi_anim_manager_trigger(&ctx.animations, adonis_dialog_animation, ADONIS_ANIMATION_SPEED, PI_INTERPOLATION_LINEAR, NULL, true);
            }
        }
        // Check if the mouse is hovering over the planet in interest
        else if (!ctx.is_paused && pi_sprite_get_distance(&ctx.van, ctx.next_planet->position.x, ctx.next_planet->position.y) < ctx.next_planet->radius + 100)
        {
            // Play an animation. This system is a silver bullet
            pi_anim_manager_trigger(&ctx.animations, completed_animation, 0.7f, PI_INTERPOLATION_QUADRATIC, NULL, false);
        }
    }
}

static void create_actors(void)
{
    // Initialize the scenes
    ctx.scenes[SCENE_LOGO] = (scene_t) { .loader = on_logo_scene_load, .renderer = on_logo_scene_render };
    ctx.scenes[SCENE_INTRODUCTION] = (scene_t) { .loader = on_introduction_scene_load, .renderer = on_introduction_scene_render };
    ctx.scenes[SCENE_SPACE] = (scene_t) { .loader = on_space_scene_load, .renderer = on_space_scene_render };

    pi_text_set_content(&ctx.introductory_text, &main_font, introduction, 20.0f, 500);
    ctx.introductory_text.position.y = 40;
    ctx.introductory_text.position.x = (WINDOW_WIDTH - ctx.introductory_text.width) / 2.0f;

    pi_sprite_create(&ctx.fuel_icon, (pi_rect_t) {96, 67, 26, 22});
    pi_sprite_set_scale(&ctx.fuel_icon, 2.0f, 2.0f);
    
    pi_sprite_create(&ctx.balance_icon, (pi_rect_t) {0, 44, 17, 30});

    pi_sprite_create(&ctx.logo, (pi_rect_t) {0, 0, 380, 424});
    pi_sprite_set_scale(&ctx.logo, 0.6f, 0.6f);
    pi_text_set_content(&ctx.logo_text, &main_font, "[Pioyi Studios]", 28.0f, -1);
    TRANSFORM_SET_X(ctx.logo.transform, WINDOW_WIDTH / 2.0f);
    TRANSFORM_SET_Y(ctx.logo.transform, WINDOW_HEIGHT / 2.0f);
    ctx.logo_text.position.y = TRANSFORM_GET_Y(ctx.logo.transform) + TRANSFORM_GET_HEIGHT(ctx.logo.transform) + 30;
    ctx.logo_text.position.x = (WINDOW_WIDTH - ctx.logo_text.width) / 2.0f;

    // Create the fuel bar, which is just an ordinary black rectangle
    pi_sprite_create_rect(&ctx.fuel_bar, 27, 17);
    ctx.active_scene = SCENE_LOGO;
    
    // Create all chunks
    for (int i = 0; i < CHUNK_ARRAY_DIM; i++)
        for (int j = 0; j < CHUNK_ARRAY_DIM; j++)
            create_chunk(i, j);

    pi_sprite_create_rect(&ctx.fade_panel, WINDOW_WIDTH, 200);
    pi_sprite_create(&ctx.adonis, (pi_rect_t) {0, 0, 70, 80});
    pi_sprite_set_scale(&ctx.adonis, 2.0f, 2.0f);
    pi_sprite_create(&ctx.van, (pi_rect_t) {43, 0, 42, 42});
    pi_sprite_set_scale(&ctx.van, 2.0f, 2.0f);

    pi_sprite_create(&ctx.location_indicator, (pi_rect_t) {49, 43, 26, 30});
    pi_sprite_create(&ctx.success_indicator, (pi_rect_t) {0, 70, 50, 55});
    pi_sprite_create(&ctx.base_fuel_sprite, (pi_rect_t) {96, 45, 27, 21});
    pi_sprite_set_scale(&ctx.base_fuel_sprite, 2.0f, 2.0f);
    pi_sprite_create(&ctx.fuel_collected, (pi_rect_t) {96, 45, 27, 21});
    pi_sprite_set_scale(&ctx.fuel_collected, 2.0f, 2.0f);
    
    // Create the smoke particle emitter
    pi_emitter_create(&ctx.van_smoke, (pi_emitter_settings_t) {
            .max_particles = 100,
            .lifetime = 0.5f,
            .velocity = {0.0f, 2.0f},
            .scale = 0.5f,
            .texture = &textures[TEXTURE_ATLAS],
            .texture_region = {84, 0, 25, 36}
        });
}

int main()
{
    ctx.window = pi_window_create(WINDOW_WIDTH, WINDOW_HEIGHT, "Zer Z600");
    pi_camera_create(&ctx.camera, WINDOW_WIDTH, WINDOW_HEIGHT);
    pi_create_root_vbo();
    pi_fonts_initialize();
    
    load_all_assets();
    pi_post_processor_create(&ctx.processor, WINDOW_WIDTH, WINDOW_HEIGHT);

    create_actors();
    ctx.scenes[ctx.active_scene].loader();
    
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
    
    // Event handling bindings
    glfwSetMouseButtonCallback(ctx.window, mouse_button_callback);
    glfwSetKeyCallback(ctx.window, key_callback);

    while (!glfwWindowShouldClose(ctx.window))
    {
        pi_post_processor_begin(&ctx.processor);
        glClear(GL_COLOR_BUFFER_BIT);

        ctx.scenes[ctx.active_scene].renderer();

        pi_anim_manager_update(&ctx.animations, DELTA_TIME);
        pi_timer_manager_update(&ctx.timers, DELTA_TIME);
        pi_post_processor_end(&ctx.processor);

        glfwSwapBuffers(ctx.window);
        glfwPollEvents();
    }

    // Clean up some resources, although that's not mandatory
    glfwDestroyWindow(ctx.window);
    glfwTerminate();
}
