/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _PI_GRAPHICS_MATH_H
#define _PI_GRAPHICS_MATH_H

#define PI 3.14159

typedef struct
{
    // Identical to an SDL_Rect
    int x, y, w, h;
} pi_rect_t;

// All matrices will be defined in column-major order
// This *might* speed things up when sending matrices as uniforms
typedef float pi_mat4_t[4][4];

typedef struct
{
    pi_mat4_t matrix;
    float rotation;
} pi_transform_t;

// Some useful macros for transform initialization
#define TRANSFORM_DISPLACEMENT(x, y) \
    { {1, 0, 0, 0},                  \
      {0, 1, 0, 0},                  \
      {0, 0, 1, 0},                  \
      {x, y, 0, 1} }

#define TRANSFORM_IDENTITY TRANSFORM_DISPLACEMENT(0, 0)

// Some macros to access transform values in a readable and abstracted manner
#define TRANSFORM_GET_X(transform) (transform.matrix[3][0])
#define TRANSFORM_GET_Y(transform) (transform.matrix[3][1])
#define TRANSFORM_SET_X(transform, value) (transform.matrix[3][0] = value)
#define TRANSFORM_SET_Y(transform, value) (transform.matrix[3][1] = value)
#define TRANSFORM_SET_WIDTH(transform, value) (transform.matrix[0][0] = value)
#define TRANSFORM_SET_HEIGHT(transform, value) (transform.matrix[1][1] = value)
#define TRANSFORM_GET_WIDTH(transform) (transform.matrix[0][0])
#define TRANSFORM_GET_HEIGHT(transform) (transform.matrix[1][1])

#define TRANSFORM_MOVE_X(transform, amount) (transform.matrix[3][0] += amount)
#define TRANSFORM_MOVE_Y(transform, amount) (transform.matrix[3][1] += amount)

typedef struct
{
    int x, y;
} pi_vec2i_t;

typedef struct
{
    float x, y;
} pi_vec2_t;

void pi_vec2_add(pi_vec2_t *dest, pi_vec2_t *first, pi_vec2_t *second);
void pi_vec2_subtract(pi_vec2_t *dest, pi_vec2_t *first, pi_vec2_t *second);
void pi_vec2_multiply_by_scalar(pi_vec2_t *dest, pi_vec2_t *src, float scalar);
float pi_vec2_get_magnitude(pi_vec2_t *vector);
void pi_vec2_normalize(pi_vec2_t *dest, pi_vec2_t *src);

#endif
