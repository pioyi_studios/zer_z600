#version 430

layout (location = 0) in vec2 v_position;
layout (location = 1) in vec2 glyph_offset;
layout (location = 2) in int texture_id;

uniform mat4 view_matrix;
uniform vec2 position;
uniform float scale;

out flat int f_texture_id;
out vec2 f_tex_coords;

void main()
{
    gl_Position = view_matrix * vec4(scale * v_position + position + glyph_offset, 0, 1);
    
    f_texture_id = texture_id;
    f_tex_coords = v_position;
}