#version 430

layout (location = 0) out vec4 f_color;

in vec2 tex_coords;
uniform sampler2D screen_texture;

float thickness = 0.02;
uniform float progress;
vec2 source = vec2(0.5, 0.5);

void main()
{
    // First, create the donut shape mask to apply the distortion
    // Get the distance from the application point
    vec2 distortion = normalize(tex_coords - source) * 0.04;
    float distance = length(tex_coords - source);

    // Create a smooth circle, logical AND it with another smaller smooth circle and get a donut
    float mask = (1.0 - smoothstep(progress - 0.1, progress, distance))
                * smoothstep(progress - thickness - 0.1, progress - thickness, distance);

    // Apply the distortion only where the mask allows for
    f_color = texture(screen_texture, tex_coords - distortion * mask);
}
