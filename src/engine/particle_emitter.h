/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _PI_PARTICLE_EMITTER_H
#define _PI_PARTICLE_EMITTER_H

#include "graphics_math.h"
#include "data_structures/dyn_array.h"
#include "textures.h"
#include "camera.h"
#include "shaders.h"

typedef struct
{
    float rotation;
    float lifetime;
    pi_vec2_t position;
} pi_particle_t;

typedef struct
{
    size_t max_particles;
    float lifetime;
    pi_vec2_t velocity;
    float scale;
    pi_texture_t *texture;
    pi_rect_t texture_region;
} pi_emitter_settings_t;

typedef struct
{
    pi_vec2_t position;
    pi_particle_t *particles;
    pi_vec2_t *particle_velocities;

    size_t active_particles;
    pi_emitter_settings_t settings;

    // Common initial scale factor for all particles
    pi_vec2_t scale;
    GLint internal_vao;
    GLint internal_vbo;
} pi_emitter_t;

void pi_emitter_create(pi_emitter_t *emitter, pi_emitter_settings_t settings);
void pi_emitter_spawn(pi_emitter_t *emitter);

void pi_emitter_update_and_render(pi_emitter_t *emitter, float delta_s, pi_transform_t *view_matrix, pi_program_t *shader);

#endif
