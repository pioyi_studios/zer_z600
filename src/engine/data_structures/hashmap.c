/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hashmap.h"
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

// I will be using the wikipedia-suggested 64-bit versions
#define FNV_OFFSET 14695981039346656037UL
#define FNV_PRIME 1099511628211UL

// This method is static because it should only be accessible within this file
static uint64_t hash_string(const char *key)
{
    // Just a simple implementation of the FNV-1a hash algorithm
    uint64_t hash = FNV_OFFSET;

    for (const char *c = key; *c; c++)
    {
        hash ^= (uint64_t) *c;
        hash *= FNV_PRIME;
    }

    return hash;
}

void pi_hashmap_create(pi_hashmap_t *hashmap)
{
    hashmap->capacity = PI_HASHMAP_INITIAL_CAPACITY;
    hashmap->length = 0;
    
    // Allocate enough memory for the entries array
    hashmap->entries = calloc(hashmap->capacity, sizeof(pi_hashmap_entry_t));
}

void* pi_hashmap_find_value_from_string(pi_hashmap_t *hashmap, const char *key)
{
    // We first need to hash the string and find its initial index
    // Getting the modulo with a perfect power of two is really simple
    //  Just zero out all bytes greater than the bit corresponding to the capacity!
    uint64_t starting_index = hash_string(key) & (hashmap->capacity - 1);
    uint64_t index = starting_index;
    
    while (hashmap->entries[index].key != NULL)
    {
        // The key might have been moved due to a collision, although unlikely
        // If that's the case, just keep incrementing the index and wrap around
        if (!strcmp(key, hashmap->entries[index].key))
            return hashmap->entries[index].value;

        index++;

        // If we've gone too far, don't bother. Inform me that I should fix something
        assert(index - starting_index <= 3);
        
        if (index >= hashmap->capacity)
            index = 0;
    }

    printf("failed to find key %s in hashmap\n", key);
    abort();
}

void pi_hashmap_insert_new_string(pi_hashmap_t *hashmap, const char *key, void *value)
{
    // If the hashmap is full, abort
    assert (hashmap->length != hashmap->capacity);

    // Pretty much the same code as before
    uint64_t starting_index = hash_string(key) & (hashmap->capacity - 1);
    uint64_t index = starting_index;

    // Find the first empty spot
    // NOTE: I won't be modifying any values throughout this project, so I'm just ignoring that!
    while (hashmap->entries[index].key != NULL)
    {
        if (index++ >= hashmap->capacity)
            index = 0;
    }

    // Make sure to copy the string on the heap
    // It might be located in the stack too, we can never be sure!
    hashmap->entries[index].key = strdup(key);
    hashmap->entries[index].value = value;

    hashmap->length++;
}

