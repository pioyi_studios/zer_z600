/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "timers.h"
#include <stdlib.h>

void pi_timer_manager_register(pi_timer_manager_t *m, float seconds, pi_timer_callback_t callback, void *data)
{
    // First, create a new timer node instance
    pi_timer_t *timer = malloc(sizeof(pi_timer_t));
    timer->callback = callback;
    timer->remaining_s = seconds;
    timer->data = data;
    
    pi_linked_list_insert(&m->active_timers, timer);
}

void pi_timer_manager_update(pi_timer_manager_t *m, float delta_s)
{
    pi_linked_node_t *node = m->active_timers;
    
    while (node != NULL)
    {
        // Update the elapsed time field
        pi_timer_t *timer = node->data;
        pi_linked_node_t *next = node->next;

        timer->remaining_s -= delta_s;

        // If the timer has finished, call the callback and delete it
        if (timer->remaining_s <= 0)
        {
            timer->callback(timer->data);
            pi_linked_list_delete(&m->active_timers, timer);
        }

        node = next;
    }
}
