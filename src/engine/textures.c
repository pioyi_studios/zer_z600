/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "textures.h"
#include <stdlib.h>
#include "common.h"
#include "qoi_image.h"

/*
 * Just load the QOI image and create an OpenGL texture out of it
 */
void pi_texture_load_qoi(pi_texture_t *texture, const char *qoi_path)
{
    qoi_image_t image;
    if (qoi_image_decode(&image, qoi_path) != 0)
        pi_log_fatal("failed to read image %s in memory", qoi_path);

    texture->internal_format = image.channels == 3 ? GL_RGB : GL_RGBA;
    texture->width = image.width;
    texture->height = image.height;
    
    glGenTextures(1, &texture->id);
    glBindTexture(GL_TEXTURE_2D, texture->id);

    // Configuring the texture's paramters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    // Copy the data
    glTexImage2D(GL_TEXTURE_2D, 0, texture->internal_format, texture->width, texture->height, 0,
                 texture->internal_format, GL_UNSIGNED_BYTE, image.data);
    
    glGenerateMipmap(GL_TEXTURE_2D);
    free(image.data);
}
