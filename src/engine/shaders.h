/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _PI_SHADERS_H
#define _PI_SHADERS_H

#include <GL/glew.h>
#include "data_structures/hashmap.h"
#include "common.h"

#define PI_SHADER_CREATE_PAIR(dst, vert_path, frag_path)                             \
    pi_shader_program_create(dst, 2, pi_shader_compile(vert_path, GL_VERTEX_SHADER), \
                             pi_shader_compile(frag_path, GL_FRAGMENT_SHADER))

#define PI_SHADER_UNIFORM(shader, key)                                     \
    *(GLint*) pi_hashmap_find_value_from_string(&(shader)->locations, key)

// Will just temrinate the program upon error
GLint pi_shader_compile(const char *shader_path, GLenum shader_type);

typedef struct
{
    GLint id;

    /*
     * Storing uniform locations so I don't have to query the GPU
     * NOTE: You could have probably made use of another data type that
     * exploits the fact that uniform locations are consecutive!
     */
    pi_hashmap_t locations;
} pi_program_t;

void pi_shader_program_create(pi_program_t *program, int total_shaders, ...);

#endif
