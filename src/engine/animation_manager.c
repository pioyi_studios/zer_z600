/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "animation_manager.h"
#include <math.h>
#include <stdlib.h>

pi_animation_t* pi_anim_manager_trigger(pi_anim_manager_t *m, pi_animation_callback_t callback,
                                        float duration, pi_interpolation_e interpolation, void *data, bool is_reversed)
{
    // First, create a new animation node instance
    pi_animation_t *anim = malloc(sizeof(pi_animation_t));
    anim->duration = duration;
    anim->interpolation = interpolation;
    anim->elapsed = 0.0f;
    anim->callback = callback;
    anim->data = data;
    anim->is_reversed = is_reversed;
    
    // Insert the animation into the manager
    pi_linked_list_insert(&m->active_animations, anim);
    anim->callback(anim, anim->is_reversed ? 1.0f : 0.0f, anim->data);

    return anim;
}

void pi_anim_manager_update(pi_anim_manager_t *m, float delta_s)
{
    pi_linked_node_t *node = m->active_animations;

    while (node != NULL)
    {
        // Update the elapsed time field
        pi_animation_t *anim = node->data;
        node = node->next;

        anim->elapsed += delta_s;
        
        // If the animation has finished, call the callback one final time and then delete
        if (anim->elapsed >= anim->duration)
        {
            anim->callback(anim, anim->is_reversed ? 0.0f : 1.0f, anim->data);
            pi_linked_list_delete(&m->active_animations, anim);
        }
        else
        {
            float progress = anim->elapsed / anim->duration;
            if (anim->is_reversed) progress = 1.0f - progress;
            
            switch (anim->interpolation)
            {
            case PI_INTERPOLATION_LINEAR: anim->callback(anim, progress, anim->data); break;
            case PI_INTERPOLATION_QUADRATIC: anim->callback(anim, progress * progress, anim->data); break;
            }
        }
    }
}

void pi_anim_manager_delete(pi_anim_manager_t *m, pi_animation_t *anim)
{
    pi_linked_list_delete(&m->active_animations, anim);
}

/*
 * Built-in animations
 */
void pi_scale_anim_callback(float time_progress, void *data)
{
    pi_scale_anim_t *anim_data = data;
    
    if (time_progress != 1.0f)
    {
        float new_scale = anim_data->initial_scale + anim_data->scale_change * sin(2 * PI * time_progress);
        pi_sprite_set_scale(anim_data->sprite, new_scale, new_scale);
    }
    // If the animation has just ended, de-allocate the data!
    else
    {
        pi_sprite_set_scale(anim_data->sprite, anim_data->initial_scale, anim_data->initial_scale);
        free(data);
    }
}
