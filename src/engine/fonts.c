/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fonts.h"
#include "common.h"
#include "shaders.h"
#include <string.h>
#include "graphics_math.h"

// Some common, static data
FT_Library ft;
GLint glyph_vbo;
pi_program_t font_shader;

void pi_fonts_initialize(void)
{
    // Just initialize the freetype liberary
    if (FT_Init_FreeType(&ft))
        pi_log_fatal("failed to initialize freetype library");

    // Create the VBO that will be common to all instanced glyphs
    GLfloat vertices[] = {
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
    };

    glGenBuffers(1, &glyph_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, glyph_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Create the common font shader
    PI_SHADER_CREATE_PAIR(&font_shader, "res/shaders/font.vert", "res/shaders/font.frag");
}

void pi_font_load_ttf(pi_font_t *font, const char *ttf_path)
{
    FT_Face face;

    // First, create the face
    if (FT_New_Face(ft, ttf_path, 0, &face))
        pi_log_fatal("failed to load face from path %s", ttf_path);

    // Set size of each glyph
    FT_Set_Pixel_Sizes(face, GLYPH_SIZE, GLYPH_SIZE);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glGenTextures(1, &font->texture_array);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D_ARRAY, font->texture_array);
    // Create at level 0, using a tiny bitmap representation
    // The last 0 corresponds to that weird "border" attribute that must always be set to zero
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R8, GLYPH_SIZE, GLYPH_SIZE, TOTAL_GLYPHS,
                 0, GL_RED, GL_UNSIGNED_BYTE, NULL);

    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    for (int i = GLYPH_OFFSET; i < GLYPH_OFFSET + TOTAL_GLYPHS; i++)
    {
        // Set the current character as the active glyph
        if (FT_Load_Char(face, i, FT_LOAD_RENDER))
            pi_log_fatal("failed to rasterize character `%c`", i);

        // Copy onto the texture array. Each glyph occupies a single z-slice
        glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i,
                        face->glyph->bitmap.width, face->glyph->bitmap.rows, 1,
                        GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);

        font->glyphs[i] = (pi_glyph_t) {
            .width = face->glyph->bitmap.width,
            .height = face->glyph->bitmap.rows,
            .bearing_x = face->glyph->bitmap_left,
            .bearing_y = face->glyph->bitmap_top,
            .advance = face->glyph->advance.x
        };
    }

    FT_Done_Face(face);
}

void pi_text_set_content(pi_text_t *text, pi_font_t *font, const char *content, float scale, float max_width)
{
    // If the content has already been set, clear the former variables
    if (text->internal_vao != 0)
    {
        glDeleteBuffers(1, &text->offsets_vbo);
        glDeleteBuffers(1, &text->ids_vbo);
        glDeleteVertexArrays(1, &text->internal_vao);
    }
    
    int offset_x = 0;
    int offset_y = 0;
    // Initial value, might change in the process
    text->width = max_width;
    
    pi_vec2_t *offsets = malloc(strlen(content) * sizeof(pi_vec2_t));
    GLint *texture_ids = malloc(strlen(content) * sizeof(GLint));
    int current_index = 0;
    
    // Fill in the batcher's data array for each character
    for (const char *c = content; *c; c++)
    {
        pi_glyph_t *glyph = &font->glyphs[*c];

        if (*c == '\n')
        {
            // If we've found a new line character, just move the offset
            offset_y += 1.1f * scale;
            // Perform a carriage return as well
            offset_x = 0;
        }
        else if (*c == ' ')
        {
            // Don't render anything, just move the offset
            offset_x += (glyph->advance >> 6) * scale / (float) GLYPH_SIZE;

            // If we've surprassed the recommended width, move to the next line
            if (max_width != -1 && offset_x > max_width)
            {
                if (offset_x > text->width)
                    text->width = offset_x;
                
                offset_x = 0;
                offset_y += 1.1f * scale;
            }
        }
        else
        {
            offsets[current_index].x = glyph->bearing_x * scale / (float) GLYPH_SIZE + offset_x;
            offsets[current_index].y = offset_y + 1.0f - glyph->bearing_y * scale / (float) GLYPH_SIZE;
            texture_ids[current_index] = *c;

            offset_x += (glyph->advance >> 6) * scale / (float) GLYPH_SIZE;

            if (offset_x > text->width)
                text->width = offset_x;

            current_index++;
        }
    }

    // Create the actual VAO that will hold all vertex buffer objects,
    // which will themselves hold all this data to be instanced
    glGenVertexArrays(1, &text->internal_vao);
    glBindVertexArray(text->internal_vao);

    // Passing in common vertex data
    glBindBuffer(GL_ARRAY_BUFFER, glyph_vbo);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*) 0);

    // Passing in unique data for each glyph
    glGenBuffers(1, &text->offsets_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, text->offsets_vbo);
    glBufferData(GL_ARRAY_BUFFER, current_index * sizeof(pi_vec2_t), offsets, GL_STATIC_DRAW);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*) 0);

    glGenBuffers(1, &text->ids_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, text->ids_vbo);
    glBufferData(GL_ARRAY_BUFFER, current_index * sizeof(GLint), texture_ids, GL_STATIC_DRAW);
    glEnableVertexAttribArray(2);
    glVertexAttribIPointer(2, 1, GL_INT, 0, (GLvoid*) 0);

    // These two will NOT be shared
    glVertexAttribDivisor(1, 1);
    glVertexAttribDivisor(2, 1);

    free(offsets);
    free(texture_ids);

    text->scale = scale;
    text->height = offset_y + scale;
    text->total_glyphs = current_index;
}

void pi_text_render(pi_text_t *text, pi_transform_t *view_matrix)
{
    glUseProgram(font_shader.id);
    glUniformMatrix4fv(PI_SHADER_UNIFORM(&font_shader, "view_matrix"), 1, GL_FALSE, (GLfloat*) view_matrix);
    glUniform2f(PI_SHADER_UNIFORM(&font_shader, "position"), text->position.x, text->position.y);
    glUniform1f(PI_SHADER_UNIFORM(&font_shader, "scale"), text->scale);
    
    glBindVertexArray(text->internal_vao);
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, text->total_glyphs);
}
