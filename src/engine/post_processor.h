/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _POST_PROCESSOR_H
#define _POST_PROCESSOR_H

#include "shaders.h"

typedef struct
{
    pi_program_t *shader;
    GLint frame_buffer;
    GLint internal_texture;
} pi_post_processor_t;

void pi_post_processor_create(pi_post_processor_t *processor, int width, int height);
void pi_post_processor_begin(pi_post_processor_t *processor);
void pi_post_processor_end(pi_post_processor_t *processor);

void pi_post_processor_activate(pi_post_processor_t *processor, pi_program_t *program);
void pi_post_processor_disable(pi_post_processor_t *processor);

#endif
