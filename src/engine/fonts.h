/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _PI_FONT_H
#define _PI_FONT_H

#include <GL/glew.h>
#include "camera.h"
#include <ft2build.h>
#include FT_FREETYPE_H

void pi_fonts_initialize(void);

#define TOTAL_GLYPHS 128
#define GLYPH_OFFSET 0
#define GLYPH_SIZE 256

typedef struct
{
    int width, height;
    // Positional offset relative to the origin
    int bearing_x, bearing_y;
    
    // Horizontal offset to reach the next glyph
    // The advance is defined as 1/64's of a pixel!
    int advance;
} pi_glyph_t;

typedef struct
{
    // Stores a rasterized bitmap of the glyphs
    // Will be used during rendering to make instancing possible
    GLint texture_array;

    pi_glyph_t glyphs[TOTAL_GLYPHS];
} pi_font_t;

void pi_font_load_ttf(pi_font_t *font, const char *ttf_path);

typedef struct
{
    // This VAO is going to be used to instance each character of the text
    // It's much more efficient than any other simple solution out there
    GLint internal_vao;
    GLint offsets_vbo, ids_vbo;

    // TODO: Make this work like any other transform
    pi_vec2_t position;
    float scale;
    
    int total_glyphs;
    int width, height;
} pi_text_t;

void pi_text_set_content(pi_text_t *text, pi_font_t *font, const char *content, float scale, float max_width);
void pi_text_render(pi_text_t *text, pi_transform_t *view_matrix);
void pi_text_set_scale(pi_text_t *text, float new_scale);

#endif
