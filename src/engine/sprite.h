/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _PI_SPRITE_H
#define _PI_SPRITE_H

#include <stdbool.h>
#include "graphics_math.h"
#include "shaders.h"
#include "camera.h"
#include "textures.h"

// Sprites will reference a texture enum identifier
// along with a rectangular area to make atlases more convenient
typedef struct
{
    pi_rect_t texture_region;
    pi_transform_t transform;
    float transparency;
} pi_sprite_t;

// Will be actually initialized on the implementation file
extern GLint pi_root_vbo;
extern GLint pi_root_vao;

void pi_create_root_vbo(void);

void pi_sprite_set_scale(pi_sprite_t *sprite, float new_x, float new_y);

void pi_sprite_create(pi_sprite_t *sprite, pi_rect_t rect);
void pi_sprite_create_rect(pi_sprite_t *sprite, float width, float height);

void pi_sprite_render(pi_sprite_t *sprite, pi_transform_t *view_matrix, pi_program_t *program);
void pi_sprite_render_textured(pi_sprite_t *sprite, pi_texture_t *texture, pi_transform_t *view_matrix, pi_program_t *program);
bool pi_sprite_is_point_inside(pi_sprite_t *sprite, float x, float y);
float pi_sprite_get_distance(pi_sprite_t *sprite, float x, float y);

#endif
