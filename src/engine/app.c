/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "app.h"
#include "common.h"
#include <time.h>
#include <stdlib.h>
#include <SDL2/SDL_mixer.h>

GLFWwindow* pi_window_create(int width, int height, const char *title)
{
    srand(time(NULL));
    if (Mix_OpenAudio(44100, AUDIO_S16SYS, 2, 512) < 0)
        pi_log_fatal("failed to initialize SDL_mixer");

    if (Mix_AllocateChannels(4) < 0)
        pi_log_fatal("failed to allocate channels");

    if (!glfwInit())
        pi_log_fatal("failed to initialize GLFW");

    // Setting some window hints to suggest the OpenGL version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_FLOATING, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
        
    // The first NULL value indicates the monitor to be used for full screen mode. This game will be windowed
    // The second NULL indicates a window with which this application will be sharing its context
    GLFWwindow *window = glfwCreateWindow(width, height, title, NULL, NULL);
    if (!window)
        pi_log_fatal("failed to create GLFW window despite the library being initialized");

    glfwMakeContextCurrent(window);
    glewInit();
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

    // Locking the swap speed based on the monitor's refresh rate
    // Specifically, we are setting the minimum number of screen updates to wait for until the buffers are swapped
    glfwSwapInterval(1);

    return window;
}
