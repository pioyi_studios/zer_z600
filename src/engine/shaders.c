/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "shaders.h"
#include "common.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

GLint pi_shader_compile(const char *shader_path, GLenum shader_type)
{
    GLint shader = glCreateShader(shader_type);

    // Loading in the shader's source code
    FILE *file = fopen(shader_path, "r");
    if (!file)
        pi_log_fatal("failed to read shader file %s", shader_path);

    // Determining the size of the file
    fseek(file, 0, SEEK_END);
    size_t file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocating enough memory for the file buffer and copying it in local memory
    GLchar *file_content = malloc(file_size + 1);
    if (fread(file_content, 1, file_size, file) != file_size)
        pi_log_fatal("failed to load the contents of %s in memory", shader_path);

    file_content[file_size] = 0;

    glShaderSource(shader, 1, (const GLchar * const *) &file_content, NULL);
    glCompileShader(shader);

    // Check for errors
    GLint was_success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &was_success);
    if (!was_success)
    {
        // Determing the error message's length
        GLint error_length;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &error_length);

        GLchar *error_message = malloc(error_length);
        GLsizei length;
        glGetShaderInfoLog(shader, error_length, &length, error_message);

        pi_log_fatal("failed to compile shader %s: %s", shader_path, error_message);
    }
    
    fclose(file);
    return shader;
}

void pi_shader_program_create(pi_program_t *program, int total_shaders, ...)
{
    va_list shaders;
    va_start(shaders, total_shaders);

    // Initializing the shader program
    program->id = glCreateProgram();

    // Attaching all shaders and then linking
    for (int i = 0; i < total_shaders; i++)
        glAttachShader(program->id, va_arg(shaders, GLint));

    glLinkProgram(program->id);

    // Check for errors
    GLint was_success;
    glGetProgramiv(program->id, GL_LINK_STATUS, &was_success);
    if (!was_success)
    {
        GLint error_length;
        glGetProgramiv(program->id, GL_INFO_LOG_LENGTH, &error_length);

        GLchar *error_message = malloc(error_length);
        GLsizei length;
        glGetProgramInfoLog(program->id, error_length, &length, error_message);

        pi_log_fatal("failed to compile shader program: %s", error_message);
    }

    va_start(shaders, total_shaders);
    
    for (int i = 0; i < total_shaders; i++)
        glDeleteShader(va_arg(shaders, GLint));
    
    GLint total_uniforms;
    glGetProgramiv(program->id, GL_ACTIVE_UNIFORMS, &total_uniforms);

    // If there are no uniforms, avoid creating that hashmap!
    if (total_uniforms == 0)
        return;
    
    /*
     * Storing the locations of each shader uniform
     * Will save up some look up time later
     */
    pi_hashmap_create(&program->locations);

    // Setting a reasonable upper limit
    GLchar uniform_name[256];
    GLsizei uniform_length;
    GLint uniform_size;
    GLenum uniform_type;
    
    for (GLuint i = 0; i < total_uniforms; i++)
    {
        glGetActiveUniform(program->id, i, 256, &uniform_length, &uniform_size, &uniform_type, uniform_name);
        
        // Check if the uniform identifier name is just too big
        if (uniform_length > 256)
            pi_log_fatal("please make your uniform variables less that 256 characters long, they look ugly");

        // Insert a NULL terminating character so that the identifier can be used just like any other string
        uniform_name[uniform_length] = 0;

        // Insert the value into the shader program's internal hashmap
        // Will need a heap allocation because I'm using a generic data type
        GLuint *value = malloc(sizeof(GLuint));
        *value = i;
        pi_hashmap_insert_new_string(&program->locations, uniform_name, value);
    }
}
