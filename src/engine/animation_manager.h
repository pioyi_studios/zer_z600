/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _ANIMATION_MANAGER_H
#define _ANIMATION_MANAGER_H

#include <stdbool.h>
#include "data_structures/linked_list.h"
#include "sprite.h"

typedef struct pi_animation_t pi_animation_t;
typedef void (*pi_animation_callback_t) (pi_animation_t *anim, float time_progress, void *data);

typedef enum
{
    PI_INTERPOLATION_LINEAR,
    PI_INTERPOLATION_QUADRATIC
} pi_interpolation_e;

typedef struct pi_animation_t
{
    pi_animation_callback_t callback;
    pi_interpolation_e interpolation;
    float duration;
    float elapsed;
    bool is_reversed;
    
    // Custom heap-allocated data
    void *data;
} pi_animation_t;

typedef struct
{
    // They should all be updated after each frame
    pi_linked_list_t active_animations;
} pi_anim_manager_t;

pi_animation_t* pi_anim_manager_trigger(pi_anim_manager_t *m, pi_animation_callback_t callback,
                                        float duration, pi_interpolation_e interpolation, void *data, bool is_reversed);

void pi_anim_manager_update(pi_anim_manager_t *m, float delta_s);
void pi_anim_manager_delete(pi_anim_manager_t *m, pi_animation_t *anim);

/*
 * A set of built-in animations
 * These are too common
 */
typedef struct
{
    pi_sprite_t *sprite;
    float initial_scale;
    float scale_change;
} pi_scale_anim_t;

void pi_scale_anim_callback(float time_progress, void *data);

#endif
