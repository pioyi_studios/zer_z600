#version 430

layout (location = 0) out vec4 f_color;

in float f_lifetime;
in vec2 f_tex_coords;
uniform sampler2D sprite_texture;

void main()
{
    f_color = texture(sprite_texture, f_tex_coords);
    
    if (f_color.a < 0.1)
        discard;

    f_color.a = smoothstep(0.0, 1.0, f_lifetime);
}