/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _PI_CAMERA_H
#define _PI_CAMERA_H

#include "graphics_math.h"
#include <stdbool.h>

typedef struct
{
    // Will be applied to all entities during the rendering phase
    pi_transform_t world_transform;
    pi_transform_t ui_transform;
} pi_camera_t;

void pi_camera_create(pi_camera_t *camera, float resolution_x, float resolution_y);
// Can be used to switch between UI and world rendering mode.

float pi_camera_get_x(pi_camera_t *camera);
float pi_camera_get_y(pi_camera_t *camera);
void pi_camera_set_position(pi_camera_t *camera, float new_x, float new_y);

#endif
