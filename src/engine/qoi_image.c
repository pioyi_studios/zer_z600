#include "qoi_image.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define QOI_OP_INDEX 0x00
// The difference of each channel will capture 2 bits
// And it will encode a value in the range[-2, 1]
#define QOI_OP_DIFF 0x40
// Same as before but each component gets 6 bits this time
#define QOI_OP_LUMA 0x80

// Used to encode repetition
// The remaining zeroed 6 bites will function as a counter 
#define QOI_OP_RUN 0xc0
#define QOI_OP_RGB 0xfe
#define QOI_OP_RGBA 0xff

#define QOI_MASK 0xc0
#define QOI_HEADER_SIZE 14

#define INDEX_LENGTH 64
#define QOI_COLOR_INDEX(c) ((c[0] * 3 + c[1] * 5 + c[2] * 7 + c[3] * 11) % INDEX_LENGTH)

// All QOI files need to be terminated with these eight bytes
static uint8_t qoi_padding[8] = {0, 0, 0, 0, 0, 0, 0, 1};

uint32_t read_32_big_endian(uint8_t *bytes)
{
    // The first byte shall have the most weight, so I move it 3 places to the right
    // A bitwise OR will stick everything together
    return bytes[0] << 24 | bytes[1] << 16 | bytes[2] << 8 | bytes[3];
}

void write_32_big_endian(uint8_t *buffer, uint32_t number)
{
    for (int i = 0; i < 4; i++)
        buffer[i] = number >> (8 * (3 - i));
}

int qoi_image_decode(qoi_image_t *image, const char *file_path)
{
    FILE *file = fopen(file_path, "r");
    if (!file)
        return -1;

    // Failure status is assumed by default
    // Following the practise of the libpng manual here
    int status = -1;
    
    // I will be first storing the file contents into memory
    // Will make management easier and will probably speed things up too
    fseek(file, 0, SEEK_END);
    size_t file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    uint8_t *buffer = malloc(file_size);
    if (!buffer)
        goto buffer_malloc_failed;

    if (fread(buffer, sizeof(uint8_t), file_size, file) != file_size)
        goto reading_buffer_failed;
    
    // Making sure that the magic signature is valid
    if (strncmp(buffer, "qoif", 4))
        goto reading_buffer_failed;

    image->width = read_32_big_endian(buffer + 4);
    image->height = read_32_big_endian(buffer + 8);
    
    // Check if something went wrong while reading
    if (image->width < 0 || image->height < 0)
        goto reading_buffer_failed;

    image->channels = buffer[12];
    image->colorspace = buffer[13];

    // Allocating memory to store the pixels
    size_t pixel_bytes = image->width * image->height * image->channels;
    image->data = malloc(pixel_bytes);
    if (!image->data)
        goto reading_buffer_failed;

    uint8_t *index_array = calloc(INDEX_LENGTH, image->channels);
    if (!index_array)
        goto image_reconstruction_failed;
    
    uint8_t last_pixel[4] = {0, 0, 0, 255};
    size_t offset = QOI_HEADER_SIZE;
    uint8_t run = 0;
    
    for (size_t pixel_offset = 0; pixel_offset < pixel_bytes; pixel_offset += image->channels)
    {
        uint8_t *pixel_location = image->data + pixel_offset;
        
        // If we're on a run loop, just replicate the previous pixel
        if (run > 0)
            run--;
        
        else
        {
            uint8_t opcode = buffer[offset++];

            // If we are receiving plain color information, just store it in
            // last_pixel will shortly after be copied onto the actual array
            //  it helps keep a reference at the previous pixel too
            if (opcode == QOI_OP_RGB || opcode == QOI_OP_RGBA)
            {
                last_pixel[0] = buffer[offset++];
                last_pixel[1] = buffer[offset++];
                last_pixel[2] = buffer[offset++];

                if (opcode == QOI_OP_RGBA)
                    last_pixel[3] = buffer[offset++];
            }
            // Check if we should enter a repetition loop
            else if ((opcode & QOI_MASK) == QOI_OP_RUN)
                run = opcode & 0x3f;
            
            else if ((opcode & QOI_MASK) == QOI_OP_INDEX)
            {
                // The opcode starts with zero anyways, so no need for bitwise operations
                memcpy(last_pixel, index_array + opcode * image->channels, image->channels);
            }
            else if ((opcode & QOI_MASK) == QOI_OP_DIFF)
            {
                // Collecting the first two bits of each component
                // Shifting the bits so that they come right at the start of the byte
                // Note the bias
                last_pixel[0] += (opcode >> 4 & 0x03) - 2;
                last_pixel[1] += (opcode >> 2 & 0x03) - 2;
                last_pixel[2] += (opcode & 0x03) - 2;
            }
            else if ((opcode & QOI_MASK) == QOI_OP_LUMA)
            {
                // The green component will capture the remainder of the first byte
                int green_difference = (opcode & 0x3f) - 32;
                last_pixel[1] += green_difference;

                uint8_t second_byte = buffer[offset++];
                // red difference = green_difference + (red_difference - green_difference)
                // subtracting eight so that it is converted into a two's complement signed number
                last_pixel[0] += green_difference + (second_byte >> 4 & 0x0f) - 8;

                last_pixel[2] += green_difference + (second_byte & 0x0f) - 8;
            }
            
            // Insert the pixel into the index
            memcpy(index_array + QOI_COLOR_INDEX(last_pixel) * image->channels,
               last_pixel, image->channels);
        }

        memcpy(pixel_location, last_pixel, image->channels);
    }

    // Free auxillary memory and return with a success status
    status = 0;

image_reconstruction_failed:
    free(index_array);
reading_buffer_failed:
    free(buffer);
buffer_malloc_failed:
    fclose(file);
    
    return status;
}

int qoi_image_encode(uint8_t *data, uint32_t width, uint32_t height, uint8_t channels, const char *file_path)
{
    int status = -1;

    // Check if the input is valid
    if (channels < 3 || channels > 4 || width < 0 || height < 0)
        return status;
    
    // Open up the target file
    FILE *file = fopen(file_path, "w");
    if (!file)
        return status;

    // Storing the results into an intermediate array so that no file is corrupted
    // If an error occured, the image will remain as is.
    // I will be allocated a safe amount of space, larger than what will be received
    uint8_t *buffer = malloc(width * height * (channels + 1)
                             + QOI_HEADER_SIZE + sizeof(qoi_padding));
    if (!buffer)
        goto buffer_malloc_failed;
    
    uint8_t *index_array = calloc(INDEX_LENGTH, channels);
    if (!index_array)
        goto index_malloc_failed;

    // Constructing the header
    strncpy(buffer, "qoif", 4);
    write_32_big_endian(buffer + 4, width);
    write_32_big_endian(buffer + 8, height);
    buffer[12] = channels;
    buffer[13] = 0;

    size_t offset = QOI_HEADER_SIZE;
    uint8_t previous_pixel[4] = {0, 0, 0, 255};
    uint8_t run = 0;
    
    for (size_t pixel_offset = 0; pixel_offset < width * height * channels; pixel_offset += channels)
    {
        uint8_t *pixel = data + pixel_offset;

        // If the pixels are identical, just increment the run
        if (!memcmp(pixel, previous_pixel, channels))
        {
            run++;

            // If we've overflowed past the maximum limit, write it to the buffer and start over
            // Run lengths of 63 and 64 would have resulted in a conflict with QOI_OP_RGB and QOI_OP_RGBA
            if (run == 62 || pixel_offset == width * height * channels - channels)
            {
                // The run is stored with a -1 bias
                buffer[offset++] = QOI_OP_RUN | (run - 1);
                run = 0;
            }
        }
        else
        {
            // Check if a run just ended, because the pixels are not equal
            if (run > 0)
            {
                buffer[offset++] = QOI_OP_RUN | (run - 1);
                run = 0;
            }

            int color_index = QOI_COLOR_INDEX(pixel);
            
            // If the pixel has not be overwritten by anything in the index, just store that integer
            // The behaviour will be replicated successfully by the decoder
            if (!memcmp(index_array + color_index * channels, pixel, channels))
            {
                buffer[offset++] = QOI_OP_INDEX | color_index;
            }
            else
            {
                // Store the pixel into the index_array
                memcpy(index_array + color_index * channels, pixel, channels);

                // If the current and previous pixels only differ by their RGB channels,
                // there might be an opportunity for a DIFF or LUMA chunk
                if (channels == 3 || pixel[3] == previous_pixel[3])
                {
                    int dr = pixel[0] - previous_pixel[0];
                    int dg = pixel[1] - previous_pixel[1];
                    int db = pixel[2] - previous_pixel[2];

                    int drg = dr - dg;
                    int dbg = db - dg;
                    
                    // Check if we are inside the DIFF limit, there's a bias of 2
                    if (dr > -3 && dr < 2 && dg > -3 && dg < 2 && db > -3 && db < 2)
                        buffer[offset++] = QOI_OP_DIFF | (dr + 2) << 4 | (dg + 2) << 2 | db + 2;
                        
                    // Check if we are inside the LUMA limit
                    else if (dg > -33 && dg < 32 && drg > -9 && drg < 8 && dbg > -9 && dbg < 8)
                    {
                        buffer[offset++] = QOI_OP_LUMA | (dg + 32);
                        buffer[offset++] = (drg + 8) << 4 | (dbg + 8);
                    }
                    else
                    {
                        // If no fancy trick worked, just store the RGB value
                        buffer[offset++] = QOI_OP_RGB;
                        memcpy(buffer + offset, pixel, 3 * sizeof(uint8_t));
                        offset += 3;
                    }
                }
                // If the alpha is different, we need to store this separately
                else if (channels == 4)
                {
                    buffer[offset++] = QOI_OP_RGBA;
                    memcpy(buffer + offset, pixel, 4 * sizeof(uint8_t));
                    offset += 4;
                }
            }

            // Keep track of the previous value
            memcpy(previous_pixel, pixel, channels * sizeof(uint8_t));
        }
    }

    // Insert the byte stream's end mark
    memcpy(buffer + offset, qoi_padding, sizeof(qoi_padding));
    offset += sizeof(qoi_padding);
    
    // Write the resulting buffer into the file
    fwrite(buffer, offset, 1, file);
    // Force the write operation
    fflush(file);
    
    // The opeartion was successful
    status = 0;
    
image_encoding_failed:
    free(index_array);
index_malloc_failed:    
    free(buffer);
buffer_malloc_failed:
    fclose(file);
    return status;
}
