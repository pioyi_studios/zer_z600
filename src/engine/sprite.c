/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sprite.h"

// All entities, including UI elements and movable sprites will be derived from this single VAO
// Then, the rectangle will be scaled according to the model matrix and the texture adjusted via a uniform
// I think this is really great. The camera will make the game resolution-independent as well
GLint pi_root_vao;
GLint pi_root_vbo;

void pi_create_root_vbo(void)
{
    GLfloat vertices[] = {
        -1.0f, -1.0f,  0.0f, 0.0f,
        -1.0f, 1.0f,   0.0f, 1.0f,
        1.0f, -1.0f,   1.0f, 0.0f,
        1.0f, 1.0f,    1.0f, 1.0f,
    };

    // The VBO can be retrieved through the VAO, no need to store that too
    glGenBuffers(1, &pi_root_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, pi_root_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glGenVertexArrays(1, &pi_root_vao);
    glBindVertexArray(pi_root_vao);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*) 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*) (2 * sizeof(GLfloat)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
}

void pi_sprite_create(pi_sprite_t *sprite, pi_rect_t rect)
{
    // Initialize transform into the identity matrix
    sprite->transform = (pi_transform_t) {
        .rotation = 0,
        .matrix = TRANSFORM_IDENTITY
    };

    // Scale the rectangle accordingly
    TRANSFORM_SET_WIDTH(sprite->transform, rect.w / 2.0f);
    TRANSFORM_SET_HEIGHT(sprite->transform, rect.h / 2.0f);

    // Store the texture region, if a texture is present
    sprite->texture_region = rect;
}

void pi_sprite_create_rect(pi_sprite_t *sprite, float width, float height)
{
    pi_sprite_create(sprite, (pi_rect_t) { 0, 0, width, height });
}

void pi_sprite_set_scale(pi_sprite_t *sprite, float new_x, float new_y)
{
    TRANSFORM_SET_WIDTH(sprite->transform, sprite->texture_region.w * new_x / 2.0f);
    TRANSFORM_SET_HEIGHT(sprite->transform, sprite->texture_region.h * new_y / 2.0f);
}

static void pi_sprite_render_internal(pi_sprite_t *sprite, pi_transform_t *view_matrix, pi_program_t *program)
{
    glUniformMatrix4fv(PI_SHADER_UNIFORM(program, "model_matrix"), 1, GL_FALSE, &sprite->transform.matrix[0][0]);
    glUniformMatrix4fv(PI_SHADER_UNIFORM(program, "view_matrix"), 1, GL_FALSE, (GLfloat*) view_matrix->matrix);
    glUniform1f(PI_SHADER_UNIFORM(program, "transparency"), sprite->transparency);
    glUniform1f(PI_SHADER_UNIFORM(program, "rotation"), sprite->transform.rotation);

    // Render the vertices as a triangle strip
    // This method can render a rectangled with only 4 vertices, instead of 6!
    glBindVertexArray(pi_root_vao);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void pi_sprite_render(pi_sprite_t *sprite, pi_transform_t *view_matrix, pi_program_t *program)
{
    glUseProgram(program->id);
    
    // Sprites can be normal rectangles too
    // Texture coordinates can then be used to form gradients!
    pi_sprite_render_internal(sprite, view_matrix, program);
}

void pi_sprite_render_textured(pi_sprite_t *sprite, pi_texture_t *texture, pi_transform_t *view_matrix, pi_program_t *program)
{
    glUseProgram(program->id);
    glBindTexture(GL_TEXTURE_2D, texture->id);

    float ndc_region[4] = {
        sprite->texture_region.x / (float) texture->width,
        sprite->texture_region.y / (float) texture->height,
        sprite->texture_region.w / (float) texture->width,
        sprite->texture_region.h / (float) texture->height,
    };

    glUniform4fv(PI_SHADER_UNIFORM(program, "texture_region"), 1, ndc_region);
    pi_sprite_render_internal(sprite, view_matrix, program);
}

bool pi_sprite_is_point_inside(pi_sprite_t *sprite, float x, float y)
{
    // Remember that the sprite's origin is located at the texture's center
    // Also, we have to keep scale into account as well
    float scale_x = TRANSFORM_GET_WIDTH(sprite->transform);
    float scale_y = TRANSFORM_GET_HEIGHT(sprite->transform);

    pi_rect_t rect = {
        TRANSFORM_GET_X(sprite->transform) - scale_x,
        TRANSFORM_GET_Y(sprite->transform) - scale_y,
        scale_x * 2.0f,
        scale_y * 2.0f
    };

    return (x >= rect.x && x <= rect.x + rect.w &&
            y >= rect.y && y <= rect.y + rect.h);
}

float pi_sprite_get_distance(pi_sprite_t *sprite, float x, float y)
{
    pi_vec2_t vector = {
        TRANSFORM_GET_X(sprite->transform) - x,
        TRANSFORM_GET_Y(sprite->transform) - y
    };

    return pi_vec2_get_magnitude(&vector);
}
