#version 430

layout (location = 0) out vec4 f_color;

in vec2 tex_coords;
uniform sampler2D screen_texture;
uniform float darkness;

void main()
{
    f_color = texture(screen_texture, tex_coords + vec2(0.06 * darkness * sin(tex_coords.y * 70 * 3.14159), 0));
    
    // Slowing darken everything based on the progress
    // Make it a bit more red than usual
    f_color.r += 0.2 * darkness;
    f_color = vec4(f_color.rgb * (1.0f - 1.05 * darkness), 1.0);
}
