# Returns all c files nested or not in $(1)
define collect_sources
	$(shell find $(1) -name '*.c')
endef

SOURCES = $(call collect_sources, src)
OBJECTS = $(patsubst %.c, objects/%.o, $(SOURCES))

LIBRARIES = glew glfw3 freetype2 SDL2_mixer
LD_FLAGS = `pkg-config --libs $(LIBRARIES)`
C_FLAGS = `pkg-config --cflags $(LIBRARIES)`

.PHONY: build
all: build

build: $(OBJECTS)
	@echo "{Makefile} Creating the executable"
	@$(CC) $(OBJECTS) -o bin $(LD_FLAGS) -lm

	@./bin

objects/%.o: %.c
	@# Making sure that the directory already exists before creating the object
	@mkdir -p $(dir $@)

	@echo "{Makefile} Building $@"
	@$(CC) -c $< -o $@ $(C_FLAGS)

