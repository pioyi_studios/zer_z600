/* Pioyi Engine
 * Copyright (C) 2024 Petros Katiforis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "graphics_math.h"
#include <math.h>

// Much faster than pow()
#define SQUARED(x) (x * x)

void pi_vec2_add(pi_vec2_t *dest, pi_vec2_t *first, pi_vec2_t *second)
{
    // TODO: Exploit SIMD instructions to make this faster
    dest->x = first->x + second->x;
    dest->y = first->y + second->y;
}

void pi_vec2_subtract(pi_vec2_t *dest, pi_vec2_t *first, pi_vec2_t *second)
{
    dest->x = first->x - second->x;
    dest->y = first->y - second->y;
}

void pi_vec2_multiply_by_scalar(pi_vec2_t *dest, pi_vec2_t *src, float scalar)
{
    dest->x = src->x * scalar;
    dest->y = src->y * scalar;
}

float pi_vec2_get_magnitude(pi_vec2_t *vector)
{
    return sqrt(SQUARED(vector->x) + SQUARED(vector->y));
}

void pi_vec2_normalize(pi_vec2_t *dest, pi_vec2_t *src)
{
    pi_vec2_multiply_by_scalar(dest, src, 1.0f / pi_vec2_get_magnitude(src));
}
